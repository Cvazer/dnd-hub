package com.gitlab.cvazer.dnd.game.model

import com.gitlab.cvazer.dnd.content.model.Background
import com.gitlab.cvazer.dnd.content.model.Cls
import com.gitlab.cvazer.dnd.content.model.Race

interface Character {
    var id: Long?
    var name: String
    var exp: Long

    var hp: Hp

    var race: CharRace
    var classes: MutableList<CharCls>
    var background: Background

    var inventories: MutableList<Inventory>

    interface CharCls {
        var level: Int
        var cls: Cls
        var subcls: Cls?
    }

    interface CharRace {
        var race: Race
        var subrace: Race?
    }

    interface Hp {
        var current: Int
        var max: Int
        var tmp: Int
    }
}