package com.gitlab.cvazer.dnd.game.model

import com.gitlab.cvazer.dnd.content.model.Item

interface Inventory {
    var name: String
    val items: MutableList<Item>
}