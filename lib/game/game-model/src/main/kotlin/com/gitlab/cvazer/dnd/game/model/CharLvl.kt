package com.gitlab.cvazer.dnd.game.model

interface CharLvl {
    val reqExp: Long
    val numeric: Int
    val profBonus: Int
}