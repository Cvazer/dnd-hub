package com.gitlab.cvazer.dnd.game.model

interface CharView {
    var name: String
    var exp: Exp
    var hp: Character.Hp
    var race: String
    var classes: Map<String, Int>
    var background: String

    interface Exp {
        val total: Long
        val nextLvlAt: Long
        val deltaReq: Long
        val deltaCurrent: Long
    }
}