package com.gitlab.cvazer.dnd.game.api.dao

import com.gitlab.cvazer.dnd.game.model.Character

interface CharDao {
    fun findById(id: Long): Character
    fun save(char: Character): Character
}