package com.gitlab.cvazer.dnd.game.api

import com.gitlab.cvazer.dnd.game.model.CharView
import com.gitlab.cvazer.dnd.game.model.Character

/**
 * Service for interacting with [Character] entities.
 * Note: Service is not intended to save (persist) character entities.
 * @param C character type extending [Character]
 * @param V view type extending [CharView]
 * @author Yan Frankovski
 */
interface CharService<C: Character, V: CharView> {

    /**
     * Creates a [V] view object from given [C] character
     * @param char a source character entity that will be used to create view
     * @return corresponding [CharView] object
     */
    fun getView(char: C): V

    /**
     * This method is used to inflict damage to [C] character entity.
     * It deducts relevant damage from temporary HP first then any leftover
     * damage is carried to character current HP.
     * Must produce [IllegalArgumentException] in case of 0 or negative [amount]
     * Note: method DOES NOT persist neither given nor resulting entities.
     * @param char [C] character entity that method will operate upon
     * @param amount amount of damage to be inflicted
     * @return entity that method operated upon
     */
    fun damage(char: C, amount: Int): C

    /**
     * Restores [C] character's current HP by given amount.
     * Hard capped at character maximum HP. Any leftover healing amount
     * will be ignored.
     * Must produce [IllegalArgumentException] in case of 0 or negative [amount]
     * Note: method DOES NOT persist neither given nor resulting entities.
     * @param char [C] character entity that method will operate upon
     * @param amount of healing
     * @return entity that method operated upon
     */
    fun heal(char: C, amount: Int): C

    /**
     * Grants [C] character given amount of temporary HP.
     * According to game rules any previous temporary HP will be
     * overridden by new amount without regard to whether or not previous
     * value was bigger or smaller than given one.
     * Must produce [IllegalArgumentException] in case of 0 or negative [amount]
     * Note: method DOES NOT persist neither given nor resulting entities.
     * @param char [C] character entity that method will operate upon
     * @param amount amount of temporary HP
     * @return entity that method operated upon
     */
    fun giveTmpHp(char: C, amount: Int): C

    /**
     * Grant specified [amount] of Exp to specified [C] character
     * Must produce [IllegalArgumentException] in case of 0 or negative [amount]
     * Note: method DOES NOT persist neither given nor resulting entities.
     * @param char [C] character entity that method will operate upon
     * @param amount of xp that should be granted
     * @return entity that method operated upon
     */
    fun giveExp(char: C, amount: Long): C

}