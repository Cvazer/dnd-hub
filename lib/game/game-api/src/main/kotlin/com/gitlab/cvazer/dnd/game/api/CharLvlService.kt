package com.gitlab.cvazer.dnd.game.api

import com.gitlab.cvazer.dnd.game.model.CharLvl

/**
 * Provides functionality to interact with character leveling
 * @author Yan Frankovski
 */
interface CharLvlService {
    /**
     * Returns [CharLvl] based on given [exp] amount.
     * Will give back closest character level based on exp amount.
     * Example: for 2nd level on 300 exp and 3rd on 900 exp method will return
     * 2nd level if given 400 exp
     * @param exp given amount of exp
     * @return corresponding [CharLvl] entity
     */
    fun getCurrent(exp: Long): CharLvl

    /**
     * Returns next [CharLvl] based on given current [exp] amount.
     * Will give back next closest character level based on exp amount.
     * Example: for 2nd level on 300 exp and 3rd on 900 exp method will return
     * 3rd level if given 400 exp
     * @param exp given amount of exp
     * @return corresponding [CharLvl] entity
     */
    fun getNext(exp: Long): CharLvl
}