package com.gitlab.cvazer.dnd.game.api.dao

import com.gitlab.cvazer.dnd.game.model.CharLvl

interface CharLvlDao {
    fun findAll(): List<CharLvl>
}