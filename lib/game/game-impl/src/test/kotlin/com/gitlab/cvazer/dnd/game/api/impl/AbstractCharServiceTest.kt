package com.gitlab.cvazer.dnd.game.api.impl

import com.gitlab.cvazer.dnd.game.api.CharLvlService
import com.gitlab.cvazer.dnd.game.api.dao.CharLvlDao
import com.gitlab.cvazer.dnd.game.api.impl.CharMocks.createChar
import com.gitlab.cvazer.dnd.game.api.impl.CharMocks.requestCharCls
import com.gitlab.cvazer.dnd.game.model.Character
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import org.junit.jupiter.params.provider.ValueSource
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Spy
import org.mockito.junit.jupiter.MockitoExtension
import org.mockito.junit.jupiter.MockitoSettings
import org.mockito.kotlin.spy
import org.mockito.kotlin.verify
import org.mockito.quality.Strictness

@ExtendWith(MockitoExtension::class)
@MockitoSettings(strictness = Strictness.LENIENT)
internal class AbstractCharServiceTest {

    private lateinit var charService: TestCharService

    @Spy
    @InjectMocks
    private lateinit var charLvlService: AbstractCharLvlServiceTest.TestCharLvlService

    @Mock
    private lateinit var charLvlDao: CharLvlDao

    @BeforeEach
    fun init() {
        charService = spy(TestCharService(charLvlService))
        AbstractCharLvlServiceTest.initCharLvlDaoMock(charLvlDao)
    }

    @ParameterizedTest
    @DisplayName("getView() check valid race name")
    @CsvSource("TestRace,", "TestRace,TestSubrace")
    fun getViewCheckRaceName(raceName: String, subRaceName: String?) {
        val view = charService.getView(
            createChar(raceName = raceName, subRaceName = subRaceName)
        )
        assertEquals(
            raceName + (subRaceName?.let { " ($subRaceName)" } ?: ""),
            view.race
        )
    }

    @ParameterizedTest
    @DisplayName("getView() check valid cls name")
    @CsvSource("TestCls,,1", "TestCls,TestSubCls,1")
    fun getViewCheckClsName(clsName: String, subClsName: String?, lvl: Int) {
        val view = charService.getView(
            createChar(charClasses = requestCharCls(clsName, subClsName, lvl))
        )
        val key = clsName + (subClsName?.let { " ($subClsName)" } ?: "")
        assertTrue(view.classes.containsKey(key))
        val resLvl = view.classes[key]
        assertEquals(lvl, resLvl)
    }

    @ParameterizedTest
    @DisplayName("getView() check valid exp values")
    @CsvSource("0,300,300,0", "400,900,600,100", "305000,355000,50000,0", "355000,355000,0,0")
    fun getViewCheckValidExpValues(exp: Long, expNext: Long, deltaTotal: Long, deltaCurrent: Long) {
        val view = charService.getView(createChar(charExp = exp))

        assertEquals(exp, view.exp.total)
        assertEquals(expNext, view.exp.nextLvlAt)
        assertEquals(deltaTotal, view.exp.deltaReq)
        assertEquals(deltaCurrent, view.exp.deltaCurrent)
    }

    @ParameterizedTest
    @DisplayName("damage() sanity check")
    @CsvSource("10,10", "10,2", "10,12")
    fun damageSuccess(hp: Int, damage: Int) {
        val char = createChar(maxHp = hp, currentHp = hp)
        charService.damage(char, damage)
        val target = if (damage >= hp) 0 else hp - damage
        assertEquals(target, char.hp.current)
    }

    @ParameterizedTest
    @DisplayName("damage() check if tmp hp gets deducted first")
    @CsvSource("10,2,4", "10,0,4", "10,5,4")
    fun damageTmpFirst(hpCurrent: Int, hpTmp: Int, damage: Int) {
        val char = createChar(maxHp = hpCurrent, currentHp = hpCurrent, tmpHp = hpTmp)
        charService.damage(char, damage)
        val targetHp = if (damage > hpTmp) hpCurrent - (damage - hpTmp) else hpCurrent
        val targetTmp = if (damage > hpTmp) 0 else hpTmp - damage
        assertEquals(targetHp, char.hp.current)
        assertEquals(targetTmp, char.hp.tmp)
    }

    @ParameterizedTest
    @DisplayName("damage() exception on invalid argument")
    @ValueSource(ints = [-10, 0])
    fun damageInvalidArgument(damage: Int) {
        val exception = assertThrows<IllegalArgumentException> {
            charService.damage(createChar(maxHp = 10, currentHp = 10), damage)
        }
        assertNotNull(exception)
        assertEquals(AbstractCharService.NON_POSITIVE_DAMAGE_ERROR_TEXT, exception.message)
    }

    @ParameterizedTest
    @DisplayName("heal() sanity check")
    @CsvSource("10,0,5", "10,8,4")
    fun healSuccess(hpMax: Int, hpCurrent: Int, heal: Int) {
        val char = createChar(maxHp = hpMax, currentHp = hpCurrent)
        charService.heal(char, heal)
        val target = if (heal + hpCurrent >= hpMax) hpMax else hpCurrent + heal
        assertEquals(target, char.hp.current)
    }

    @ParameterizedTest
    @DisplayName("heal() exception on invalid argument")
    @ValueSource(ints = [-10, 0])
    fun healInvalidArgument(heal: Int) {
        val exception = assertThrows<IllegalArgumentException> {
            charService.heal(createChar(maxHp = 10, currentHp = 1), heal)
        }
        assertNotNull(exception)
        assertEquals(AbstractCharService.NON_POSITIVE_HEAL_ERROR_TEXT, exception.message)
    }

    @ParameterizedTest
    @DisplayName("giveTmpHp() sanity check")
    @CsvSource("20,12", "2,5")
    fun giveTmpHpSanity(first: Int, second: Int) {
        val char = createChar()
        charService.giveTmpHp(char, first)
        assertEquals(first, char.hp.tmp)
        charService.giveTmpHp(char, second)
        assertEquals(second, char.hp.tmp)
    }

    @ParameterizedTest
    @DisplayName("giveTmpHp() exception on invalid argument")
    @ValueSource(ints = [-10, 0])
    fun giveTmpHpInvalidArgument(hp: Int) {
        val exception = assertThrows<IllegalArgumentException> {
            charService.giveTmpHp(createChar(), hp)
        }
        assertNotNull(exception)
        assertEquals(AbstractCharService.NON_POSITIVE_TMP_ERROR_TEXT, exception.message)
    }

    @Test
    @DisplayName("giveExp() sanity check")
    fun giveExpSanity(){
        val char = charService.giveExp(createChar(charExp = 0), 10)
        verify(char).exp = 10
    }

    @ParameterizedTest
    @DisplayName("giveExp() exception on invalid argument")
    @ValueSource(longs = [-10, 0])
    fun giveExpInvalidArgument(exp: Long) {
        val exception = assertThrows<IllegalArgumentException> {
            charService.giveExp(createChar(), exp)
        }
        assertNotNull(exception)
        assertEquals(AbstractCharService.NON_POSITIVE_EXP_ERROR_TEXT, exception.message)
    }

    open class TestCharService(
        override val charLvlService: CharLvlService
    ): AbstractCharService<Character>(charLvlService)
}