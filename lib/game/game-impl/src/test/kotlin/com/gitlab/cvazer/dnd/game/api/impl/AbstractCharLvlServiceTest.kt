package com.gitlab.cvazer.dnd.game.api.impl

import com.gitlab.cvazer.dnd.game.api.dao.CharLvlDao
import com.gitlab.cvazer.dnd.game.api.impl.CharLvlMocks.createCharLvl
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Spy
import org.mockito.junit.jupiter.MockitoExtension
import org.mockito.junit.jupiter.MockitoSettings
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.whenever
import org.mockito.quality.Strictness

@ExtendWith(MockitoExtension::class)
@MockitoSettings(strictness = Strictness.LENIENT)
internal class AbstractCharLvlServiceTest {

    @Spy
    @InjectMocks
    private lateinit var charLvlService: TestCharLvlService

    @Mock
    private lateinit var charLvlDao: CharLvlDao

    @BeforeEach
    fun init() {
        initCharLvlDaoMock(charLvlDao)
    }


    @ParameterizedTest
    @DisplayName("getCurrent() sanity check")
    @CsvSource("0,1", "355000,20", "300,2", "400,2")
    fun getCurrentSanity(exp: Long, expectedLvl: Int) {
        val lvl = charLvlService.getCurrent(exp)
        assertEquals(expectedLvl, lvl.numeric)
    }

    @ParameterizedTest
    @DisplayName("getNext() sanity check")
    @CsvSource("0,2", "355000,", "300,3")
    fun getNextSanity(exp: Long, expectedLvl: Int?) {
        val lvl = charLvlService.getNext(exp)
        if (expectedLvl == null) {
            val current = charLvlService.getCurrent(exp)
            assertEquals(current.numeric, lvl.numeric)
        } else {
            assertEquals(expectedLvl, lvl.numeric)
        }
    }

    open class TestCharLvlService(
        override val charLvlDao: CharLvlDao
    ): AbstractCharLvlService(charLvlDao)

    companion object {
        fun initCharLvlDaoMock(charLvlDao: CharLvlDao) {
            val list = listOf(
                createCharLvl(),
                createCharLvl(300, 2, 2),
                createCharLvl(900, 3, 2),
                createCharLvl(305_000, 19, 6),
                createCharLvl(355_000, 20, 6),
            )
            doReturn(list).whenever(charLvlDao).findAll()
        }
    }
}