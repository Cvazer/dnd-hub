package com.gitlab.cvazer.dnd.game.api.impl

import com.gitlab.cvazer.dnd.game.model.CharLvl
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock

object CharLvlMocks {
    internal fun createCharLvl(
        exp: Long = 0,
        num: Int = 1,
        prof: Int = 2
    ): CharLvl = mock {
        on { reqExp } doReturn exp
        on { numeric } doReturn num
        on { profBonus } doReturn prof
    }
}