package com.gitlab.cvazer.dnd.game.api.impl

import com.gitlab.cvazer.dnd.content.model.Background
import com.gitlab.cvazer.dnd.content.model.Cls
import com.gitlab.cvazer.dnd.content.model.Race
import com.gitlab.cvazer.dnd.game.model.Character
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock

internal object CharMocks {
    internal fun createChar(
        charName: String = "DefaultCharName",
        raceName: String = "DefaultRaceName",
        subRaceName: String? = null,
        charExp: Long = 0,
        bgName: String = "DefaultBgName",
        maxHp: Int = 0,
        currentHp: Int = 0,
        tmpHp: Int = 0,
        charClasses: List<Pair<String, Int>> = requestCharCls()
    ): Character {
        val backgroundMock: Background = mock {
            on { name } doReturn bgName
        }
        val raceMock: Race = mock {
            on { name } doReturn raceName
        }
        val subraceMock: Race? = subRaceName?.let { mock {
            on { name } doReturn subRaceName
        } }
        val charRaceMock: Character.CharRace = mock {
            on { race } doReturn raceMock
            on { subrace } doReturn subraceMock
        }
        val classesMock = createCharClasses(charClasses)
        return mock {
            on { name } doReturn charName
            on { race } doReturn charRaceMock
            on { exp } doReturn charExp
            on { background } doReturn backgroundMock
            on { hp } doReturn TestHpIml(currentHp, maxHp, tmpHp)
            on { classes } doReturn classesMock
        }
    }

    internal fun createCharClass(clsName: String, subclsName: String?, lvl: Int): MutableList<Character.CharCls> =
        createCharClasses(listOf(Pair(clsName+","+(subclsName ?: ""), lvl)))
    internal fun createCharClass(names: String, lvl: Int): MutableList<Character.CharCls> =
        createCharClasses(listOf(Pair(names, lvl)))
    internal fun createCharClass(data: Pair<String, Int>): MutableList<Character.CharCls> =
        createCharClasses(listOf(data))
    internal fun createCharClasses(classes: List<Pair<String, Int>>): MutableList<Character.CharCls> {
        return classes.map { data ->
            val namesSplit = data.first.split(",")
            val clsMock: Cls = mock {
                on { name } doReturn namesSplit[0]
            }
            val subClsMock: Cls? = if (namesSplit.size == 2 && namesSplit[1] != "") {
                mock {
                    on { name } doReturn namesSplit[1]
                }
            } else null
            return@map mock<Character.CharCls> {
                on { cls } doReturn clsMock
                on { subcls } doReturn subClsMock
                on { level } doReturn data.second
            }
        }.toMutableList()
    }

    internal fun requestCharCls(
        clsName: String = "DefaultClsName",
        subclsName: String? = null,
        lvl: Int = 1
    ): List<Pair<String, Int>> {
        return listOf(Pair(clsName+","+(subclsName ?: ""), lvl))
    }

    open class TestHpIml(
        override var current: Int,
        override var max: Int,
        override var tmp: Int
        ): Character.Hp
}