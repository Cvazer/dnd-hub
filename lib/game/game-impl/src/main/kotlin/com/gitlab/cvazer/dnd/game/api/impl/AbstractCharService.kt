package com.gitlab.cvazer.dnd.game.api.impl

import com.gitlab.cvazer.dnd.game.api.CharLvlService
import com.gitlab.cvazer.dnd.game.api.CharService
import com.gitlab.cvazer.dnd.game.model.CharView
import com.gitlab.cvazer.dnd.game.model.Character
import com.gitlab.cvazer.dnd.game.model.impl.CharViewImpl

abstract class AbstractCharService<C: Character>(
    protected open val charLvlService: CharLvlService
): CharService<C, CharViewImpl> {
    override fun getView(char: C): CharViewImpl {
        val view = CharViewImpl(
            name = char.name,
            exp = getExp(char),
            hp = char.hp,
            race = char.race.race.name + (char.race.subrace?.let { " (${it.name})" } ?: ""),
            classes = char.classes.associate { Pair(
                    it.cls.name + (it.subcls?.let { s -> " (${s.name})" } ?: ""),
                    it.level
            ) },
            background = char.background.name
        )
        return view
    }

    private fun getExp(char: C): CharView.Exp {
        val next = charLvlService.getNext(char.exp).reqExp
        val current = charLvlService.getCurrent(char.exp).reqExp
        return CharViewImpl.ExpImpl(
            total = char.exp,
            nextLvlAt = next,
            deltaReq = next - current,
            deltaCurrent = (next - current) - (next - char.exp)
        )
    }

    override fun damage(char: C, amount: Int): C {
        if (amount <= 0) throw IllegalArgumentException(NON_POSITIVE_DAMAGE_ERROR_TEXT)
        var allDamage = amount

        if (allDamage <= char.hp.tmp) {
            char.hp.tmp -= allDamage
            return char
        } else {
            allDamage -= char.hp.tmp
            char.hp.tmp = 0
        }

        return if (allDamage >= char.hp.current) {
            char.hp.current = 0
            char
        } else {
            char.hp.current -= allDamage
            char
        }
    }

    override fun heal(char: C, amount: Int): C {
        if (amount <= 0) throw IllegalArgumentException(NON_POSITIVE_HEAL_ERROR_TEXT)
        char.hp.current += amount
        if (char.hp.current > char.hp.max) char.hp.current = char.hp.max
        return char
    }

    override fun giveTmpHp(char: C, amount: Int): C {
        if (amount <= 0) throw IllegalArgumentException(NON_POSITIVE_TMP_ERROR_TEXT)
        char.hp.tmp = amount
        return char
    }

    override fun giveExp(char: C, amount: Long): C {
        if (amount <= 0) throw IllegalArgumentException(NON_POSITIVE_EXP_ERROR_TEXT)
        char.exp += amount
        return char
    }

    companion object {
        @JvmStatic val NON_POSITIVE_DAMAGE_ERROR_TEXT = "Damage amount should be greater then 0"
        @JvmStatic val NON_POSITIVE_HEAL_ERROR_TEXT = "Healing amount should be greater then 0"
        @JvmStatic val NON_POSITIVE_TMP_ERROR_TEXT = "Temporary HP amount should be greater then 0"
        @JvmStatic val NON_POSITIVE_EXP_ERROR_TEXT = "Exp amount should be greater then 0"
    }
}