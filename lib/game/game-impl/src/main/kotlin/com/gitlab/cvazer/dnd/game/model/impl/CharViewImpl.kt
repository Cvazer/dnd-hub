package com.gitlab.cvazer.dnd.game.model.impl

import com.gitlab.cvazer.dnd.game.model.CharView
import com.gitlab.cvazer.dnd.game.model.Character

data class CharViewImpl(
    override var name: String,
    override var exp: CharView.Exp,
    override var hp: Character.Hp,
    override var race: String,
    override var classes: Map<String, Int>,
    override var background: String
): CharView {
    data class ExpImpl(
        override val total: Long,
        override val nextLvlAt: Long,
        override val deltaReq: Long,
        override val deltaCurrent: Long
    ): CharView.Exp
}