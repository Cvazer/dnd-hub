package com.gitlab.cvazer.dnd.game.api.impl

import com.gitlab.cvazer.dnd.game.api.CharLvlService
import com.gitlab.cvazer.dnd.game.api.dao.CharLvlDao
import com.gitlab.cvazer.dnd.game.model.CharLvl

abstract class AbstractCharLvlService(
    protected open val charLvlDao: CharLvlDao
): CharLvlService {

    override fun getCurrent(exp: Long): CharLvl = charLvlDao.findAll()
        .filter { it.reqExp <= exp }
        .maxBy { it.reqExp }

    override fun getNext(exp: Long): CharLvl {
        val current = getCurrent(exp)
        return charLvlDao.findAll()
            .find { it.numeric == current.numeric + 1 }
            ?: current
    }

}