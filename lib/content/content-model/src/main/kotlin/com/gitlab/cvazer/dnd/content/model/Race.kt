package com.gitlab.cvazer.dnd.content.model

interface Race {
    var name: String

    var subraces: MutableList<Race>
}