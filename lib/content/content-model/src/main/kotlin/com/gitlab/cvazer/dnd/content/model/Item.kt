package com.gitlab.cvazer.dnd.content.model

interface Item {
    var name: String
}