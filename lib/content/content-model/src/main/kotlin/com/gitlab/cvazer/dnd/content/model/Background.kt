package com.gitlab.cvazer.dnd.content.model

interface Background {
    var name: String
}