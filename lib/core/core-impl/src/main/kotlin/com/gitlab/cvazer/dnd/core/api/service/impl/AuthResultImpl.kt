package com.gitlab.cvazer.dnd.core.api.service.impl

import com.gitlab.cvazer.dnd.core.model.auth.AuthResult
import com.gitlab.cvazer.dnd.core.model.user.User

class AuthResultImpl<D>(
    override val data: D,
    override val status: AuthResult.Status,
    override val error: String? = null,
    override val user: User? = null
) : AuthResult<D> {
    override fun onSuccess(block: (user: User) -> Unit) {
        if (status == AuthResult.Status.OK) block.invoke(user!!)
    }

    override fun <T> mapSuccess(block: (user: User) -> T): T? =
        if (status == AuthResult.Status.OK) block.invoke(user!!) else null

    override fun <T> mapFailure(block: (result: AuthResult<D>) -> T): T? =
        if (status == AuthResult.Status.ERROR) block.invoke(this) else null

    override fun onFailure(block: (result: AuthResult<D>) -> Unit) {
        if (status == AuthResult.Status.ERROR) block.invoke(this)
    }

    companion object {
        fun <D> error(data: D, error: String) = AuthResultImpl(
            data, AuthResult.Status.ERROR, error,
        )

        fun <D> ok(data: D, user: User) = AuthResultImpl(
            data, AuthResult.Status.OK, null, user
        )
    }
}