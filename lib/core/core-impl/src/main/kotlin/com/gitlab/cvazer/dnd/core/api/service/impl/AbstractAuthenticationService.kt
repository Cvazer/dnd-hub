package com.gitlab.cvazer.dnd.core.api.service.impl

import com.gitlab.cvazer.dnd.core.api.service.AuthenticationService
import com.gitlab.cvazer.dnd.core.api.exception.AuthenticationException
import com.gitlab.cvazer.dnd.core.model.auth.AuthResult
import com.gitlab.cvazer.dnd.core.model.user.User

abstract class AbstractAuthenticationService<D, A>: AuthenticationService<D> {

    override fun authenticate(data: D): AuthResult<D> {
        val attempt = createAuthAttempt(data)

        try {
            validateAuthenticationData(data)
        } catch (e: AuthenticationException) {
            return fail(attempt, AuthResultImpl.error(data, e.message ?: VALIDATION_ERROR))
        }

        val user: User = try {
            fetchUserByAuthData(data)
        } catch (e: AuthenticationException) {
            return fail(attempt, AuthResultImpl.error(data, e.message ?: FETCH_USER_ERROR))
        } ?: return fail(attempt, AuthResultImpl.error(data, USER_NOT_FOUND_ERROR))

        return success(attempt, AuthResultImpl.ok(data, user))
    }

    private fun fail(attempt: A?, result: AuthResult<D>): AuthResult<D> {
        attempt?.also { persistFailedAttempt(result, it) }
        return result
    }

    private fun success(attempt: A?, result: AuthResult<D>): AuthResult<D> {
        attempt?.also { persistSuccessAttempt(result, it) }
        return result
    }

    @Suppress("UNUSED_PARAMETER", "MemberVisibilityCanBePrivate")
    protected fun createAuthAttempt(data: D): A? = null

    @Suppress("UNUSED_PARAMETER", "MemberVisibilityCanBePrivate")
    protected fun persistFailedAttempt(result: AuthResult<D>, attempt: A) {}

    @Suppress("UNUSED_PARAMETER", "MemberVisibilityCanBePrivate")
    protected fun persistSuccessAttempt(result: AuthResult<D>, attempt: A) {}

    protected abstract fun fetchUserByAuthData(data: D): User?

    @Throws(AuthenticationException::class)
    protected abstract fun validateAuthenticationData(data: D)

    companion object {
        @JvmStatic val VALIDATION_ERROR = "Invalid authentication data"
        @JvmStatic val FETCH_USER_ERROR = "Error during fetching user bu data"
        @JvmStatic val USER_NOT_FOUND_ERROR = "No user found by given data"
    }
}