package com.gitlab.cvazer.dnd.core.api.exception

class AuthenticationException: Exception {
    constructor(message: String): super(message)
    constructor(cause: Throwable): super(cause)
    constructor(message: String, cause: Throwable): super(message, cause)
}