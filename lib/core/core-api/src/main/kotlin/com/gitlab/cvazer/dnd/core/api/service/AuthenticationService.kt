package com.gitlab.cvazer.dnd.core.api.service

import com.gitlab.cvazer.dnd.core.model.auth.AuthResult

interface AuthenticationService<D> {
    fun authenticate(data: D): AuthResult<D>
}