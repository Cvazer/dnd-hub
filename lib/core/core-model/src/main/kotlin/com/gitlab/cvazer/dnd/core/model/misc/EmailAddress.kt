package com.gitlab.cvazer.dnd.core.model.misc

interface EmailAddress {
    val local: String
    val domain: String
}