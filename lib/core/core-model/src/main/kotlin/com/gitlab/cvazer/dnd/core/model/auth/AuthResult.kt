package com.gitlab.cvazer.dnd.core.model.auth

import com.gitlab.cvazer.dnd.core.model.user.User

interface AuthResult<D> {
    val data: D
    val status: Status
    val error: String?
    val user: User?

    fun onSuccess(block: (user: User) -> Unit)
    fun <T> mapSuccess(block: (user: User) -> T): T?
    fun onFailure(block: (result: AuthResult<D>) -> Unit)
    fun <T> mapFailure(block: (result: AuthResult<D>) -> T): T?

    enum class Status {
        OK, ERROR
    }
}