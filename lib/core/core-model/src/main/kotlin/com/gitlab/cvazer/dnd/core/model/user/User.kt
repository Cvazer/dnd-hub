package com.gitlab.cvazer.dnd.core.model.user

import java.time.OffsetDateTime

interface User {
    var id: Long?
    var nickname: String
    var created: OffsetDateTime
}