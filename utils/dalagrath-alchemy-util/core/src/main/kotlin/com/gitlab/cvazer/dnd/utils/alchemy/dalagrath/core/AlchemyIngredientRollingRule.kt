package com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core

import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.model.AlchemyIngredientRollingContext

interface AlchemyIngredientRollingRule {
    val name: String
    val description: String

    fun apply(result: AlchemyIngredientRollResult, context: AlchemyIngredientRollingContext): Boolean
}