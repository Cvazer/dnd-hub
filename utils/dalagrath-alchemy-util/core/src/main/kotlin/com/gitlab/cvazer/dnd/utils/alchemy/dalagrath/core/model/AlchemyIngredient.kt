package com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.model

interface AlchemyIngredient {

    val name: String
    val rarity: AlchemyIngredientRarity
    val details: String?
    val dc: Int
    val terrains: List<TerrainDescriptor>
    val types: List<AlchemyIngredientType>

    data class TerrainDescriptor(
        val terrain: AlchemyIngredientTableTerrain,
        val minRoll: Int = 2,
        val maxRoll: Int = 12,
        val rule: String? = null
    )
}