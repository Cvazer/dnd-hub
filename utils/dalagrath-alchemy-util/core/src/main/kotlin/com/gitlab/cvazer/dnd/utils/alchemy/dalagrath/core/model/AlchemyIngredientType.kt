package com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.model

interface AlchemyIngredientType {
    val id: String
    val name: String
}