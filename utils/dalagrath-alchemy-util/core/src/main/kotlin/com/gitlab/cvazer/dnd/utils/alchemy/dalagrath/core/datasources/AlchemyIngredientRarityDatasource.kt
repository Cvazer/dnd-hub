package com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.datasources

import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.model.AlchemyIngredientRarity

interface AlchemyIngredientRarityDatasource {
    fun getAll(): List<AlchemyIngredientRarity>
    fun findById(id: String): AlchemyIngredientRarity?
    fun findByName(name: String): AlchemyIngredientRarity?
}