package com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core

import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.model.AlchemyIngredient

interface AlchemyIngredientRollResult {
    var ingredient: AlchemyIngredient?
    var amount: Int

    var ruleText: String?
    var additionalResult: MutableList<Pair<AlchemyIngredient, Int>>
    var finalAction: FinalAction

    var roll: Int
    var elementalRoll: Int
    var initialAmount: Int

    enum class FinalAction {
        FINISH, REROLL
    }
}