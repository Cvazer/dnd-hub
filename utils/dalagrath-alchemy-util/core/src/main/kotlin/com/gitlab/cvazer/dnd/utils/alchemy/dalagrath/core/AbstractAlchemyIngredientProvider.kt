package com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core

import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.datasources.AlchemyIngredientDatasource
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.datasources.AlchemyIngredientTableTerrainDatasource
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.model.AlchemyIngredient
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.model.AlchemyIngredientRollingContext
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.model.AlchemyIngredientTableTerrain

abstract class AbstractAlchemyIngredientProvider<
        Result: AlchemyIngredientRollResult
        >: AlchemyIngredientProvider<Result> {

    override fun roll(
        terrainId: String,
        context: AlchemyIngredientRollingContext
    ): Result {
        return roll(
            getTerrainDatasource().findById(terrainId) ?: error("No terrain with id [$terrainId]"),
            context
        )
    }

    override fun roll(
        terrain: AlchemyIngredientTableTerrain,
        context: AlchemyIngredientRollingContext
    ): Result {
        val amount = (1 until 5).random()
        val elementalRoll = (1 until 101).random()
        return if (elementalRoll >= 75) {
            getElementalWaterResult(amount)
        } else {
            getNormalResult(amount, terrain, context)
        }.apply { this.elementalRoll = elementalRoll }
    }

    private fun getElementalWaterResult(amount: Int): Result {
        return getBlankResult()
            .apply { ingredient = getIngredientsDatasource().getElementalWater() }
            .apply { this.amount = amount }
            .apply { this.initialAmount = amount }
    }

    private fun getNormalResult(
        amount: Int,
        terrain: AlchemyIngredientTableTerrain,
        context: AlchemyIngredientRollingContext
    ): Result {
        val roll = (2 until 13).random()

        val (ingredient, descriptor) = getIngredientForRoll(roll, terrain)
            ?: getIngredientForRoll(roll, getTerrainDatasource().getCommon())
            ?: error("Unable to get ingredient for roll [$roll] and terrain [${terrain.id}]")

        val result = getBlankResult()
            .apply { this.ingredient = ingredient }
            .apply { this.amount = amount }
            .apply { this.roll = roll }
            .apply { this.initialAmount = amount }

        descriptor.rule
            ?.let { getRules().find { it.name == descriptor.rule } ?: error("No rule [${descriptor.rule}]") }
            ?.also { result.ruleText = it.description }
            ?.apply(result, context)

        return if (result.finalAction == AlchemyIngredientRollResult.FinalAction.REROLL) {
            getNormalResult(amount, terrain, context)
        } else {
            result
        }
    }

    private fun getIngredientForRoll(roll: Int, terrain: AlchemyIngredientTableTerrain): Pair<AlchemyIngredient, AlchemyIngredient.TerrainDescriptor>? {
        for (ingredient in getIngredientsDatasource().getAll()) {
            val descriptor = ingredient.terrains.asSequence()
                .filter { it.terrain.id == terrain.id }
                .filter { roll >= it.minRoll && roll <= it.maxRoll }
                .firstOrNull()
            if (descriptor != null) return Pair(ingredient, descriptor)
        }
        return null
    }

    abstract fun getBlankResult(): Result
    abstract fun getTerrainDatasource(): AlchemyIngredientTableTerrainDatasource
    abstract fun getIngredientsDatasource(): AlchemyIngredientDatasource
    abstract fun getRules(): List<AlchemyIngredientRollingRule>
}