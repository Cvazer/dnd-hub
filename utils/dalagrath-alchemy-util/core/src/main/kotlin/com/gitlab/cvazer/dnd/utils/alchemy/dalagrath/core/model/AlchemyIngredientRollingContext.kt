package com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.model

interface AlchemyIngredientRollingContext {
    var timeOfDay: TimeOfDay
    var consideredCave: Boolean
    var weather: Weather

    enum class TimeOfDay {
        DAY, NIGHT
    }

    enum class Weather {
        CLEAR, RAIN
    }
}