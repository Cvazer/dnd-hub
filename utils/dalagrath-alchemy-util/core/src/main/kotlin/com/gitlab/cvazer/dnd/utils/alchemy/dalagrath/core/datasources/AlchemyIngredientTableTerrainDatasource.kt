package com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.datasources

import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.model.AlchemyIngredientTableTerrain

interface AlchemyIngredientTableTerrainDatasource {
    fun getAll(): List<AlchemyIngredientTableTerrain>
    fun findById(id: String): AlchemyIngredientTableTerrain?
    fun findByName(name: String): AlchemyIngredientTableTerrain?
    fun getCommon(): AlchemyIngredientTableTerrain
}