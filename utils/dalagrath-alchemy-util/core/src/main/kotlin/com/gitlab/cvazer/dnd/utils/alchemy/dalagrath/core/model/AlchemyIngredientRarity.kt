package com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.model

interface AlchemyIngredientRarity {
    val id: String
    val name: String
    val minPrice: Long
    val maxPrice: Long
    val priceMod: Int
    val sellingPeriodMax: Int
}