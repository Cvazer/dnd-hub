package com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.model

interface AlchemyIngredientTableTerrain {
    val id: String
    val name: String
    val selectable: Boolean
}