package com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.datasources

import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.model.AlchemyIngredient

interface AlchemyIngredientDatasource {
    fun getAll(): List<AlchemyIngredient>
    fun getElementalWater(): AlchemyIngredient
}