package com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core

import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.model.AlchemyIngredientRollingContext
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.model.AlchemyIngredientTableTerrain

interface AlchemyIngredientProvider<Result: AlchemyIngredientRollResult> {

    fun roll(terrainId: String, context: AlchemyIngredientRollingContext): Result
    fun roll(terrain: AlchemyIngredientTableTerrain, context: AlchemyIngredientRollingContext): Result

}