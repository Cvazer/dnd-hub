package com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.datasources

import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.model.AlchemyIngredientType

interface AlchemyIngredientTypeDatasource {
    fun getAll(): List<AlchemyIngredientType>
    fun findById(id: String): AlchemyIngredientType?
    fun findByName(name: String): AlchemyIngredientType?
}