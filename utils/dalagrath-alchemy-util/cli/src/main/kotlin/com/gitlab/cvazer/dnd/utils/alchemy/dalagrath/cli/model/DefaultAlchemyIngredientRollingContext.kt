package com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.cli.model

import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.model.AlchemyIngredientRollingContext
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.model.AlchemyIngredientRollingContext.*

class DefaultAlchemyIngredientRollingContext(
    override var timeOfDay: TimeOfDay = TimeOfDay.DAY,
    override var consideredCave: Boolean = false,
    override var weather: Weather = Weather.CLEAR
): AlchemyIngredientRollingContext