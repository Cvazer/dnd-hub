package com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.cli.model

import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.model.AlchemyIngredientTableTerrain

class DefaultAlchemyIngredientTableTerrain(
    override val id: String,
    override val name: String,
    override val selectable: Boolean = true
): AlchemyIngredientTableTerrain