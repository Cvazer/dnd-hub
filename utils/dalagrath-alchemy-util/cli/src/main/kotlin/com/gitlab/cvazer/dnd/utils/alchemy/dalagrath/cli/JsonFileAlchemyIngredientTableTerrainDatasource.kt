package com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.cli

import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.cli.model.DefaultAlchemyIngredientTableTerrain
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.datasources.AlchemyIngredientTableTerrainDatasource
import java.io.File

class JsonFileAlchemyIngredientTableTerrainDatasource(data: String): AlchemyIngredientTableTerrainDatasource {
    constructor(file: File): this(Application.readFromFile(file))

    private val terrains: List<DefaultAlchemyIngredientTableTerrain> =
        (jacksonObjectMapper().readTree(data)["terrainTypes"] as ArrayNode)
            .iterator().asSequence()
            .map { node -> DefaultAlchemyIngredientTableTerrain(
                id = node["id"].asText(),
                name = node["name"].textValue(),
                selectable = node["selectable"].asBoolean()
            ) }.toList()

    override fun getAll() = terrains
    override fun findById(id: String) = terrains.find { it.id == id }
    override fun findByName(name: String) = terrains.find { it.name == name }
    override fun getCommon() = terrains.find { it.id == "COMMON" }!!

}