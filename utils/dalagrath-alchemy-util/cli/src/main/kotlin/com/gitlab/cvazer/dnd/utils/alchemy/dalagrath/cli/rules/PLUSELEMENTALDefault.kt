package com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.cli.rules

import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.AlchemyIngredientRollResult
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.AlchemyIngredientRollingRule
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.model.AlchemyIngredientRollingContext
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.cli.JsonFileAlchemyIngredientDatasource

class PLUSELEMENTALDefault(
    private val ingredientDatasource: JsonFileAlchemyIngredientDatasource
): AlchemyIngredientRollingRule {
    override val name: String = "PLUSELEMENTAL"
    override val description: String = "Come with 1 Elemental Water"

    override fun apply(result: AlchemyIngredientRollResult, context: AlchemyIngredientRollingContext): Boolean {
        result.additionalResult.add(Pair(ingredientDatasource.getElementalWater(), 1))
        return true
    }
}