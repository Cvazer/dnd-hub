package com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.cli.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.model.AlchemyIngredient

data class DefaultAlchemyIngredient(
    override val name: String,
    override val rarity: DefaultAlchemyIngredientRarity,
    override val details: String? = null,
    override val dc: Int = 0,
    @JsonIgnore override val terrains: List<AlchemyIngredient.TerrainDescriptor> = emptyList(),
    @JsonIgnore override val types: List<DefaultAlchemyIngredientType> = emptyList()
): AlchemyIngredient