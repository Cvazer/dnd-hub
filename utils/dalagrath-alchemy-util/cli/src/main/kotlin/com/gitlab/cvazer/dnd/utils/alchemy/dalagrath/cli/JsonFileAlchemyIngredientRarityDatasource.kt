package com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.cli

import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.cli.model.DefaultAlchemyIngredientRarity
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.datasources.AlchemyIngredientRarityDatasource
import java.io.File

class JsonFileAlchemyIngredientRarityDatasource(data: String) : AlchemyIngredientRarityDatasource {
    constructor(file: File): this(Application.readFromFile(file))

    private val rarities: List<DefaultAlchemyIngredientRarity> = (jacksonObjectMapper()
        .readTree(data)["ingredientRarities"] as ArrayNode)
        .asIterable().asSequence().map { node -> DefaultAlchemyIngredientRarity(
            id = node["id"].asText(),
            name = node["name"].asText(),
            minPrice = node["minPrice"].asLong(),
            maxPrice = node["maxPrice"].asLong(),
            priceMod = node["priceMod"].asInt(),
            sellingPeriodMax = node["sellingPeriodMax"].asInt()
        ) }.toList()


    override fun getAll() = rarities
    override fun findById(id: String) = rarities.find { it.id == id }
    override fun findByName(name: String) = rarities.find { it.name == name }
}