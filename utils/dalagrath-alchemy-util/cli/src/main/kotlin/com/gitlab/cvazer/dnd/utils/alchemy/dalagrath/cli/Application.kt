package com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.cli

import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.cli.model.DefaultAlchemyIngredientRollingContext
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.model.AlchemyIngredientRollingContext
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.model.AlchemyIngredientTableTerrain
import org.apache.commons.cli.CommandLine
import org.apache.commons.cli.DefaultParser
import org.apache.commons.cli.Options
import java.io.File
import java.util.*

private val DEFAULT_TIME_OF_DAY = AlchemyIngredientRollingContext.TimeOfDay.DAY
private val DEFAULT_WEATHER = AlchemyIngredientRollingContext.Weather.CLEAR
private const val DEFAULT_CONSIDERED_CAVE = false

fun main(args: Array<String>){
    val cliOptions = Options()

    cliOptions.addOption("T", "time-of-day", true, "Current time of day")
    cliOptions.addOption("w", "weather", true, "Current weather")
    cliOptions.addOption("c", "is-cave", false, "Is considered cave")
    cliOptions.addOption("f", "file", true, "Json file path")
    cliOptions.addOption("t", "terrain", true, "Terrain id")
    cliOptions.addOption("l", "list", false, "List all terrain ids")
    cliOptions.addOption("h", "help", false, "List all commands")

    val cmd = DefaultParser().parse(cliOptions, args)

    val context = DefaultAlchemyIngredientRollingContext(
        timeOfDay = getTimeOfDay(cmd),
        consideredCave = getIsConsideredCave(cmd),
        weather = getWeather(cmd)
    )

    val provider = JsonFileAlchemyIngredientProvider(getData(cmd))

    if (cmd.hasOption("h")) {
        for (option in cliOptions.options) {
            println("-${option.opt} --${option.longOpt} ${option.description}")
        }
        return
    }

    if (cmd.hasOption("l")) {
        provider.getTerrainDatasource().getAll()
            .forEach { println(it.id) }
        return
    }

    val terrain = getTerrain(cmd, provider)
    val res = provider.roll(terrain, context)
    println(res.prettyString())
}

fun getTerrain(cmd: CommandLine, provider: JsonFileAlchemyIngredientProvider): AlchemyIngredientTableTerrain {
    val input = cmd.getOptionValue("t")
    if (input != null) { provider.getTerrainDatasource().findById(input)!! }
    val options = provider.getTerrainDatasource().getAll().filter { it.selectable }
    return choose(options, options[0], { "Choose terrain type" }) { it.name }
}

private fun getData(cmd: CommandLine): String {
    return if (cmd.getOptionValue("f") != null) {
        return Application.readFromFile(File(cmd.getOptionValue("f")))
    } else {
        val file = File("alchemy.json")
        if (!file.exists()) {
            String(Application::class.java.classLoader.getResourceAsStream("alchemy.json")!!.readAllBytes())
        } else {
            Application.readFromFile(file)
        }
    }
}

private fun isSilent(cmd: CommandLine): Boolean {
    return cmd.getOptionValue("s") != null || cmd.hasOption("l")
}

private fun <R> ifNotSilent(cmd: CommandLine, default: R, block: (cmd: CommandLine) -> R?): R {
    return if (isSilent(cmd)) default else block.invoke(cmd) ?: default
}

private fun getTimeOfDay(cmd: CommandLine): AlchemyIngredientRollingContext.TimeOfDay {
    return ifNotSilent(cmd, DEFAULT_TIME_OF_DAY) { c ->
        c.getOptionValue("T")
            ?.let { AlchemyIngredientRollingContext.TimeOfDay.valueOf(it) }
            ?: choose(
                AlchemyIngredientRollingContext.TimeOfDay.values().asList(),
                DEFAULT_TIME_OF_DAY,
                { "What time of day is it?" }
            ) { it.toString().normalize() }
    }
}

private fun getIsConsideredCave(cmd: CommandLine): Boolean {
    return ifNotSilent(cmd, DEFAULT_CONSIDERED_CAVE) {
        if (it.getOptionValue("c") != null) return@ifNotSilent true
        yesOrNo(false) { "Is current location is considered a cave?" }
    }
}

private fun getWeather(cmd: CommandLine): AlchemyIngredientRollingContext.Weather {
    return ifNotSilent(cmd, DEFAULT_WEATHER) {c ->
        c.getOptionValue("w")
            ?.let { AlchemyIngredientRollingContext.Weather.valueOf(it) }
            ?: choose(
                AlchemyIngredientRollingContext.Weather.values().asList(),
                DEFAULT_WEATHER,
                { "What is the weather right now?" }
            ) { it.toString().normalize() }
    }
}

private fun <R> choose(
    options: List<R>,
    default: R,
    prompt: () -> String,
    transformer: (target: R) -> String = { it.toString() }
): R {
    val promptStr = prompt.invoke()
    println(promptStr + if (promptStr.matches("^.*[?.:;]+$".toRegex())) "" else ":")
    for ((i, option) in options.withIndex()) {
        println("\t${i+1}: "+transformer.invoke(option))
    }
    print("Enter option number (${transformer.invoke(default)}): ")
    var choice: R? = null
    while (true) {
        try {
            val input = readlnOrNull() ?: break
            if (input.isBlank()) break
            if (!input.trim().matches("^\\d+$".toRegex())) error("Invalid input")
            choice = options[input.toInt()-1]
            break
        } catch (ignored: Exception) {
            print("Invalid input, try again: ")
        }
    }
    return choice ?: default
}

private fun yesOrNo(default: Boolean, prompt: () -> String): Boolean {
    val res = choose(listOf("Yes", "No"), if (default) "Yes" else "No", prompt)
    return res == "Yes"
}

object Application {
    fun readFromFile(file: File) = file.readText()
}

fun String.normalize(): String {
    return this.lowercase().replaceFirstChar {
        if (it.isLowerCase()) it.titlecase(Locale.getDefault())
        else it.toString()
    }
}
