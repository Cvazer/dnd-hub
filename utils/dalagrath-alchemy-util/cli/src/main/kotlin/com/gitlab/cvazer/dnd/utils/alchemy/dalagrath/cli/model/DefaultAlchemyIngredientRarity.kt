package com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.cli.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.model.AlchemyIngredientRarity

class DefaultAlchemyIngredientRarity(
    @JsonIgnore override val id: String,
    override val name: String,
    @JsonIgnore override val minPrice: Long = 0,
    @JsonIgnore override val maxPrice: Long = 10000000,
    @JsonIgnore override val priceMod: Int = 0,
    @JsonIgnore override val sellingPeriodMax: Int = 12
): AlchemyIngredientRarity