package com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.cli

import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.cli.model.DefaultAlchemyIngredient
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.datasources.AlchemyIngredientDatasource
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.model.AlchemyIngredient
import java.io.File

class JsonFileAlchemyIngredientDatasource(
    data: String,
    private val rarityDatasource: JsonFileAlchemyIngredientRarityDatasource =
        JsonFileAlchemyIngredientRarityDatasource(data),
    private val terrainDatasource: JsonFileAlchemyIngredientTableTerrainDatasource =
        JsonFileAlchemyIngredientTableTerrainDatasource(data),
    private val typeDatasource: JsonFileAlchemyIngredientTypeDatasource =
        JsonFileAlchemyIngredientTypeDatasource(data)
): AlchemyIngredientDatasource {
    constructor(file: File): this(Application.readFromFile(file))

    private val ingredients: List<DefaultAlchemyIngredient> =
        (jacksonObjectMapper().readTree(data)["ingredients"] as ArrayNode)
            .asSequence().map { node -> DefaultAlchemyIngredient(
                name = node["name"].asText(),
                rarity = rarityDatasource.findById(node["rarityId"].asText())!!,
                details = node["details"].asText(),
                dc = node["dc"].asInt(),
                terrains = (node["terrainIds"] as ArrayNode).asSequence()
                    .map { AlchemyIngredient.TerrainDescriptor(
                        terrain = terrainDatasource.findById(it["terrainId"].asText())!!,
                        minRoll = it["minRoll"].asInt(),
                        maxRoll = it["maxRoll"].asInt(),
                        rule = it["rule"]?.asText()
                    ) }.toList(),
                types = (node["typeIds"] as ArrayNode).asSequence()
                    .map { typeDatasource.findById(it.asText())!! }
                    .toList()
            ) }.toList()

    override fun getAll() = ingredients
    override fun getElementalWater() = ingredients.find { it.name == "Elemental Water" }!!
}