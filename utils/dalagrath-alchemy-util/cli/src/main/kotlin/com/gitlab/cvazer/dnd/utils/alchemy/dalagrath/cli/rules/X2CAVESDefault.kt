package com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.cli.rules

import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.AlchemyIngredientRollResult
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.AlchemyIngredientRollingRule
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.model.AlchemyIngredientRollingContext

class X2CAVESDefault: AlchemyIngredientRollingRule {
    override val name: String = "X2CAVES"
    override val description: String = "Find 2x the rolled amount in Caves"

    override fun apply(result: AlchemyIngredientRollResult, context: AlchemyIngredientRollingContext): Boolean {
        if (context.consideredCave) {
            result.amount *= 2
        }
        return true
    }
}