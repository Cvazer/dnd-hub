package com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.cli

import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.AbstractAlchemyIngredientProvider
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.cli.model.DefaultAlchemyIngredientRollResult
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.cli.rules.*
import java.io.File

class JsonFileAlchemyIngredientProvider(data: String): AbstractAlchemyIngredientProvider<
        DefaultAlchemyIngredientRollResult
        >()
{
    constructor(file: File): this(Application.readFromFile(file))

    private val ingredientDatasource = JsonFileAlchemyIngredientDatasource(data)
    private val terrainDatasource = JsonFileAlchemyIngredientTableTerrainDatasource(data)

    override fun getBlankResult() = DefaultAlchemyIngredientRollResult()
    override fun getTerrainDatasource() = terrainDatasource
    override fun getIngredientsDatasource() = ingredientDatasource

    override fun getRules() = listOf(
        PLUSELEMENTALDefault(getIngredientsDatasource()),
        X1ORX2Default(),
        X2CAVESDefault(),
        X2Default(),
        X2NIGHTORREROLLDefault(),
        X2RAINDefault()
    )
}