package com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.cli

import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.cli.model.DefaultAlchemyIngredientType
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.datasources.AlchemyIngredientTypeDatasource
import java.io.File

class JsonFileAlchemyIngredientTypeDatasource(data: String): AlchemyIngredientTypeDatasource {
    constructor(file: File): this(Application.readFromFile(file))

    private val types: List<DefaultAlchemyIngredientType> =
        (jacksonObjectMapper().readTree(data)["ingredientTypes"] as ArrayNode)
            .iterator().asSequence()
            .map { node -> DefaultAlchemyIngredientType(
                id = node["id"].asText(),
                name = node["name"].textValue(),
            ) }.toList()

    override fun getAll() = types
    override fun findById(id: String) = types.find { it.id == id }
    override fun findByName(name: String) = types.find { it.name == name }
}