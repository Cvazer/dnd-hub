package com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.cli.model

import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.model.AlchemyIngredientType

class DefaultAlchemyIngredientType(
    override val id: String,
    override val name: String
): AlchemyIngredientType