package com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.cli.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.AlchemyIngredientRollResult
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.AlchemyIngredientRollResult.FinalAction
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.AlchemyIngredientRollResult.FinalAction.*
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.model.AlchemyIngredient
import java.lang.StringBuilder

class DefaultAlchemyIngredientRollResult(
    override var ingredient: AlchemyIngredient? = null,
    override var amount: Int = 0,

    override var ruleText: String? = null,
    override var additionalResult: MutableList<Pair<AlchemyIngredient, Int>> = mutableListOf(),
    @JsonIgnore override var finalAction: FinalAction = FINISH,

    @JsonIgnore override var roll: Int = 0,
    @JsonIgnore override var elementalRoll: Int = 0,
    @JsonIgnore  override var initialAmount: Int = 0
) : AlchemyIngredientRollResult {

    fun prettyString(): String {
        val builder = StringBuilder().append("\nYou get $amount ${ingredient!!.name}.\n")
        if (ruleText != null) builder.append(ruleText+"\n")
        builder.append("Rarity: ${ingredient!!.rarity.name}\n")
        builder.append("DC: ${ingredient!!.dc}\n")
        builder.append(ingredient!!.details+"\n")
        if (additionalResult.isNotEmpty()) {
            builder.append("Additionally you get:\n")
            for (pair in additionalResult) {
                builder.append("\t${pair.second} ${pair.first.name} " +
                        "(${pair.first.rarity}, DC ${pair.first.dc}): ${pair.first.details}")
            }
            builder.append("\n")
        }
        builder.append("------------------------------------------------------------------------\n")
        builder.append("rolled: $roll\t\telementalRoll: $elementalRoll\t\tinitial:$initialAmount\n\n\n")
        return builder.toString()
    }

    fun jsonString(): String = jacksonObjectMapper()
        .writerWithDefaultPrettyPrinter()
        .writeValueAsString(this)

}