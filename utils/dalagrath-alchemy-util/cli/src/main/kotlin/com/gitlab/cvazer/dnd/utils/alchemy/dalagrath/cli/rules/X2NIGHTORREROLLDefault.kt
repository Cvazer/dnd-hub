package com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.cli.rules

import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.AlchemyIngredientRollResult
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.AlchemyIngredientRollingRule
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.model.AlchemyIngredientRollingContext

class X2NIGHTORREROLLDefault: AlchemyIngredientRollingRule {
    override val name: String = "X2NIGHTORREROLL"
    override val description: String = "Find 2x during Night, Re-roll during Day"

    override fun apply(result: AlchemyIngredientRollResult, context: AlchemyIngredientRollingContext): Boolean {
        if (context.timeOfDay == AlchemyIngredientRollingContext.TimeOfDay.NIGHT) {
            result.amount *= 2
        } else {
            result.finalAction = AlchemyIngredientRollResult.FinalAction.FINISH
        }
        return true
    }
}