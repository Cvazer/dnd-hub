package com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.cli.rules

import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.AlchemyIngredientRollResult
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.AlchemyIngredientRollingRule
import com.gitlab.cvazer.dnd.utils.alchemy.dalagrath.core.model.AlchemyIngredientRollingContext

class X2Default: AlchemyIngredientRollingRule {
    override val name: String = "X2"
    override val description: String = "Find 2x the rolled amount"

    override fun apply(result: AlchemyIngredientRollResult, context: AlchemyIngredientRollingContext): Boolean {
        result.amount *= 2
        return true
    }

}