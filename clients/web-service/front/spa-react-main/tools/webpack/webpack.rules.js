// noinspection WebpackConfigHighlighting

const { inDev } = require('./webpack.helpers');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = [
  {
    // Typescript loader
    test: /\.tsx?$/,
    exclude: /(node_modules|\.webpack)/,
    use: {
      loader: 'ts-loader',
      options: {
        transpileOnly: true,
      },
    },
  },
  {
    // CSS Loader
    test: /\.css$/,
    use: [
      { loader: inDev() ? 'style-loader' : MiniCssExtractPlugin.loader },
      { loader: 'css-loader' },
    ],
  },
  {
    // Less loader
    test: /\.less$/,
    use: [
      { loader: inDev() ? 'style-loader' : MiniCssExtractPlugin.loader },
      { loader: 'css-loader' },
      { loader: 'less-loader' },
    ],
  },
  {
    // Images Loader
    test: /\.(gif|jpe?g|tiff|png|webp|bmp)$/,
    use: [
      {
        loader: 'file-loader',
        options: {
          publicPath: 'assets/images',
          outputPath: 'assets/images',
        },
      }
    ],
  },
  {
    test: /\.(svg$)/,
    use: [
      {
        loader: "@svgr/webpack",
        options: {
          typescript: true,
          ext: "tsx",
        }
      }
    ]
  },
  {
    // Font & SVG loader
    test: /\.(woff(2)?|ttf|otf|eot)$/,
    use: [
      {
        loader: 'file-loader',
        options: {
          publicPath: 'assets/fonts',
          outputPath: 'assets/fonts',
        },
      },
    ],
  },
];
