import { configureStore } from '@reduxjs/toolkit'
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
import accountReducer from '@src/store/account/AccountReduxSlice'

const reduxStore = configureStore({
  reducer: {
    account: accountReducer
  }
})

export default reduxStore;

export type RootState = ReturnType<typeof reduxStore.getState>
export type TsDispatch = typeof reduxStore.dispatch

export const useTsDispatch: () => TsDispatch = useDispatch<TsDispatch>;
export const useTsSelector: TypedUseSelectorHook<RootState> = useSelector