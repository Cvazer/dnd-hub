import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { User } from '@src/store/account/User';
import { errorInfo } from '@src/logic/backend/BackendService';

interface AccountState {
  token: string | null,
  user: User | null | undefined
}

const initialState: AccountState = {
  token: null,
  user: undefined
}

const accountSlice = createSlice({
  name: "account",
  initialState: initialState,
  reducers: {
    setToken: (state, action: PayloadAction<string>) => {
      if (action.payload === "null") throw errorInfo("Invalid token (null)")
      state.token = action.payload
    },
    setUser: (state, action: PayloadAction<User>) => {
      state.user = action.payload
    }
  }
})

export const { setToken, setUser } = accountSlice.actions
export default accountSlice.reducer
