import React, { StrictMode } from 'react';
import { createRoot } from 'react-dom/client';
import Application from '@comp/Application';
import { Provider } from 'react-redux';
import 'normalize.css';

import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/popover2/lib/css/blueprint-popover2.css';

import reduxStore from '@src/store/ReduxConfig';
import { BrowserRouter } from 'react-router-dom';

import { FocusStyleManager, Position, Toaster } from '@blueprintjs/core';
import { QueryClientProvider } from 'react-query';
import { queryClient } from '@src/ReqctQueryConfig';
FocusStyleManager.onlyShowFocusOnTabs();

export const AppToaster = Toaster.create({
  // className: "recipe-toaster",
  position: Position.TOP,
});

createRoot(document.getElementById('app')).render(
  <StrictMode>
    <QueryClientProvider client={queryClient}>
      <Provider store={reduxStore}>
        <BrowserRouter>
          <Application />
        </BrowserRouter>
      </Provider>
    </QueryClientProvider>
  </StrictMode>
)