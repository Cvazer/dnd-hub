import { UserCredentialsDto } from '@comp/account/settings/credentials/CredentialsSegmentFactory';
import { BackendRequestBuilder } from '@src/logic/backend/BackendRequestBuilder';
import { BackendEndpoint } from '@src/logic/backend/BackendEndpoints';
import { errorInfo } from '@src/logic/backend/BackendService';

export interface ICredentialsService {
  createBasic(username: string, password: string): Promise<UserCredentialsDto>
  setBasic(oldPassword: string, username?: string, newPassword?: string): Promise<UserCredentialsDto>
  linkGoogle(): Promise<UserCredentialsDto>
  disconnectGoogle(): Promise<never>
}

type CreateBasicRq = {
  username: string,
  password: string
}

type SetBasicRq = {
  username?: string,
  newPassword?: string
  oldPassword: string
}

class CredentialsService implements ICredentialsService {

  createBasic(username: string, password: string): Promise<UserCredentialsDto> {
    if (username === "" || password === "") {
      return Promise.reject(errorInfo("Username and Password fields " +
        "can't be empty"))
    }
    return new BackendRequestBuilder<CreateBasicRq>()
      .endpoint(BackendEndpoint.API_CORE_CREDENTIALS_BASIC)
      .body({
        username: username,
        password: password
      }).post()
  }

  disconnectGoogle(): Promise<never> {
    return Promise.reject("NYI");
  }

  linkGoogle(): Promise<UserCredentialsDto> {
    return Promise.reject("NYI");
  }

  setBasic(oldPassword: string, username?: string, newPassword?: string): Promise<UserCredentialsDto> {
    if (username === "" && newPassword === "") {
      return Promise.reject(errorInfo("No data to set"))
    } else if (oldPassword === "") {
      return Promise.reject(errorInfo("You need to provide" +
        " current password"))
    }
    return new BackendRequestBuilder<SetBasicRq>()
      .endpoint(BackendEndpoint.API_CORE_CREDENTIALS_BASIC)
      .body({
        username: username,
        newPassword: newPassword,
        oldPassword: oldPassword
      }).put()
  }

}

const credentialsService = new CredentialsService()
export default credentialsService