import axios, { AxiosRequestConfig } from 'axios';
import loginService from '@src/logic/LoginService';
import { BackendRequestBuilder } from '@src/logic/backend/BackendRequestBuilder';

const proto = "http"
const host = "localhost"
const port: string | "" = "8110"

export type ErrorInfo = {
  code: number,
  description?: string
}

type ServiceResponse<T> = {
  errorInfo: ErrorInfo,
  data?: T
}

type AxiosResponse<R> = {
  data: R
  status: number
}

export const errorInfo: (text: string) => ErrorInfo = (text) => {
  return {
    code: 1,
    description: text
  } as ErrorInfo
}

export class BackendService {

  static createUrl(url: string): string {
    return `${
      proto
    }://${
      host
    }${
      port === "" ? "" : `:${port}`
    }${
      url.startsWith("/") ? url : `/${url}` 
    }`
  }

  private async handle<R>(
    request: Promise<AxiosResponse<ServiceResponse<R>>>,
    errorHandler: (e: AxiosResponse<ServiceResponse<R>>) => Promise<R> | null = null
  ): Promise<R> {
    try {
      const res = await request;
      if (res.status !== 200) return Promise.reject(res.status)
      if (res.data.errorInfo.code !== 0) { // noinspection ExceptionCaughtLocallyJS
        throw res.data.errorInfo
      }
      return Promise.resolve(res.data.data)
    } catch (e) {
      console.debug(e);
      if (errorHandler) {
        return errorHandler(e)
      } else {
        return Promise.reject(e)
      }
    }
  }

  async get<R>(
    url: string,
    config: AxiosRequestConfig,
    refresh: boolean
  ): Promise<R> {
    return refresh
      ? this.withRefresh(config, (c) => this.handle(
        axios.get<ServiceResponse<R>>(url, c)
      ))
      : this.handle(axios.get<ServiceResponse<R>>(url, config))
  }

  async delete<R>(
    url: string,
    config: AxiosRequestConfig,
    refresh: boolean
  ): Promise<R> {
    return refresh
      ? this.withRefresh(config, (c) => this.handle(
        axios.delete<ServiceResponse<R>>(url, c)
      ))
      : this.handle(axios.delete<ServiceResponse<R>>(url, config))
  }

  async put<R, P>(
    url: string,
    payload: P,
    config: AxiosRequestConfig,
    refresh: boolean
  ): Promise<R> {
    return refresh
      ? this.withRefresh(config,(c) => this.handle(
        axios.put<ServiceResponse<R>>(url, payload, c)
      ))
      : this.handle(axios.put<ServiceResponse<R>>(url, payload, config))
  }

  async post<R, P>(
    url: string,
    payload: P,
    config: AxiosRequestConfig,
    refresh: boolean
  ): Promise<R> {
    return refresh
      ? this.withRefresh(config,(c) => this.handle(
        axios.post<ServiceResponse<R>>(url, payload, c)
      ))
      : this.handle(axios.post<ServiceResponse<R>>(url, payload, config))
  }

  private async withRefresh<R>(
    config: AxiosRequestConfig,
    attempt: (config: AxiosRequestConfig) => Promise<R>
  ): Promise<R> {
    try {
      return Promise.resolve(await attempt(config))
    } catch (e) {
      if (e.code !== 499) return Promise.reject(e)
      if (!config.headers || !config.headers.Authorization) return Promise.reject(e)
      try {
        // const oldToken = reduxStore.getState().account.token;
        const refreshedToken = await loginService.refreshToken()
        BackendRequestBuilder.setAuthToken(config, refreshedToken)
        return attempt(config)
      } catch (re) {
        console.debug("unable to refresh JWT: ", re);
        return Promise.reject(e);
      }
    }
  }
}

const backendService = new BackendService()
export default backendService