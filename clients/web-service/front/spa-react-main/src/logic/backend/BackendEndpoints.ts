export enum BackendEndpoint {
  CORE_LOGIN_BASIC = "/core/login/basic",
  CORE_LOGIN_GOOGLE = "/core/login/google",
  CORE_LOGIN_REFRESH = "/core/login/refresh",
  CORE_LOGIN_USER = "/core/login/user",
  API_CORE_USER = "/api/core/user",
  API_CORE_CREDENTIALS = "/api/core/credentials",
  API_CORE_CREDENTIALS_BASIC = "/api/core/credentials/basic",
  API_CORE_CREDENTIALS_GOOGLE = "/api/core/credentials/google",
  CORE_CHECK = "/core/check",
}