import { BackendEndpoint } from '@src/logic/backend/BackendEndpoints';
import backendService, { BackendService } from '@src/logic/backend/BackendService';
import { AxiosRequestConfig, AxiosRequestHeaders } from 'axios';
import reduxStore from '@src/store/ReduxConfig';

export class BackendRequestBuilder<T> {
  private _body: T
  private _omitAuthData: boolean
  private _noRefresh: boolean
  private _endpoint: BackendEndpoint
  private _config: AxiosRequestConfig
  private _headers: AxiosRequestHeaders


  constructor() {
    this._noRefresh = false
  }

  private _defaultConfig: AxiosRequestConfig = {
    headers: {
      "Content-Type": "application/json"
    }
  }

  body(body: T): BackendRequestBuilder<T> {
    this._body = body
    return this;
  }

  omitAuthData(): BackendRequestBuilder<T> {
    this._omitAuthData = true
    return this;
  }

  endpoint(endpoint: BackendEndpoint): BackendRequestBuilder<T> {
    this._endpoint = endpoint
    return this
  }

  noRefresh(): BackendRequestBuilder<T> {
    this._noRefresh = true
    return this
  }

  config(config: AxiosRequestConfig): BackendRequestBuilder<T> {
    this._config = config
    return this;
  }

  header(key: string, value: string): BackendRequestBuilder<T> {
    this._headers[key] = value
    return this;
  }

  get<R>(): Promise<R> {
    return backendService.get(
      BackendService.createUrl(this._endpoint),
      this.finalizeConfig(),
      !this._noRefresh
    )
  }

  delete<R>(): Promise<R> {
    return backendService.delete(
      BackendService.createUrl(this._endpoint),
      this.finalizeConfig(),
      !this._noRefresh
    )
  }

  put<R>(): Promise<R> {
    return backendService.put(
      BackendService.createUrl(this._endpoint),
      this._body,
      this.finalizeConfig(),
      !this._noRefresh
    )
  }

  post<R>(): Promise<R> {
    return backendService.post(
      BackendService.createUrl(this._endpoint),
      this._body,
      this.finalizeConfig(),
      !this._noRefresh
    )
  }

  private finalizeConfig(): AxiosRequestConfig {
    let config = { ...this._defaultConfig }

    //mix in user config if exists
    if (this._config) {
      config = { ...config, ...this._config}
    }

    //omit auth if true
    if (!this._omitAuthData) {
      BackendRequestBuilder.setAuthToken(
        config,
        reduxStore.getState().account.token
      )
    }

    //mix in headers
    config.headers = { ...config.headers, ...this._headers}

    return config
  }

  static setAuthToken(config: AxiosRequestConfig, token: string) {
    config.headers.Authorization = `Bearer ${token}`
  }

}