import reduxStore from '@src/store/ReduxConfig';
import { setToken, setUser } from '@src/store/account/AccountReduxSlice';
import { BackendRequestBuilder } from '@src/logic/backend/BackendRequestBuilder';
import { BackendEndpoint } from '@src/logic/backend/BackendEndpoints';
import userService from '@src/logic/UserService';

export interface ILoginService {
  getGoogleLoginUrl(): Promise<string>

  logInWithGoogleCode(code: string): Promise<void>

  logInWithBasic(
    username: string,
    password: string,
    rememberMe: boolean
  ): Promise<void>

  logInWithOldToken(oldToken: string): Promise<void>

  refreshToken(oldToken?: string): Promise<string>
}

class LoginService implements ILoginService{

  getGoogleLoginUrl(): Promise<string> {
    return new BackendRequestBuilder()
      .endpoint(BackendEndpoint.CORE_LOGIN_GOOGLE)
      .omitAuthData()
      .get()
  }

  async logInWithBasic(
    username: string,
    password: string,
    remember: boolean
  ): Promise<void> {
    try {
      const token = await new BackendRequestBuilder()
        .omitAuthData()
        .endpoint(BackendEndpoint.CORE_LOGIN_BASIC)
        .body({
          username: username,
          password: password,
          rememberMe: remember
        }).post<string>()
      await this.setJwtToken(token)
      const user = await userService.getCurrentUser()
      reduxStore.dispatch(setUser(user))
      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  }

  async logInWithGoogleCode(code: string): Promise<void> {
    try {
      const token = await new BackendRequestBuilder()
        .omitAuthData()
        .endpoint(BackendEndpoint.CORE_LOGIN_GOOGLE)
        .body(code)
        .post<string>()
      await this.setJwtToken(token)
      const user = await userService.getCurrentUser()
      reduxStore.dispatch(setUser(user))
      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  }

  async refreshToken(oldToken?: string): Promise<string> {
    try {
      const token = await new BackendRequestBuilder()
        .omitAuthData()
        .noRefresh()
        .endpoint(BackendEndpoint.CORE_LOGIN_REFRESH)
        .body( oldToken ? oldToken : reduxStore.getState().account.token)
        .post<string>()
      await this.setJwtToken(token)
      return Promise.resolve(token)
    } catch (e) {
      return Promise.reject(e)
    }
  }

  private async setJwtToken(token: string) {
    localStorage.setItem("jwt", token)
    await reduxStore.dispatch(setToken(token))
  }

  async logInWithOldToken(oldToken: string): Promise<void> {
    try {
      const token = await this.refreshToken(oldToken)
      await this.setJwtToken(token)
      const user = await userService.getCurrentUser()
      await reduxStore.dispatch(setUser(user))
      return Promise.resolve()
    } catch (e) {
      return Promise.reject(e)
    }
  }
}

const loginService = new LoginService()
export default loginService