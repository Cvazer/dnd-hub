import { User } from '@src/store/account/User';
import { BackendRequestBuilder } from '@src/logic/backend/BackendRequestBuilder';
import { BackendEndpoint } from '@src/logic/backend/BackendEndpoints';
import reduxStore from '@src/store/ReduxConfig';
import { setToken, setUser } from '@src/store/account/AccountReduxSlice';
import { IRegisterData } from '@comp/login/RegisterUser';
import loginService from '@src/logic/LoginService';

export interface IUserService {
  getCurrentUser(): Promise<User>
  register(data: IRegisterData): Promise<void>
  logOut(): Promise<void>
}

class UserService implements IUserService {

  async getCurrentUser(): Promise<User> {
    let user = reduxStore.getState().account.user;
    if (user == null) {
      user = await this.fetchCurrentUser()
      reduxStore.dispatch(setUser(user))
    }
    return Promise.resolve(user)
  }

  private fetchCurrentUser(): Promise<User> {
    return new BackendRequestBuilder()
      .endpoint(BackendEndpoint.API_CORE_USER)
      .get()
  }

  async logOut(): Promise<void> {
    reduxStore.dispatch(setUser(undefined))
    reduxStore.dispatch(setToken(null))
    localStorage.removeItem("jwt")
  }

  async register(data: IRegisterData): Promise<void> {
    try {
      await new BackendRequestBuilder()
        .endpoint(BackendEndpoint.CORE_LOGIN_USER)
        .omitAuthData()
        .body(data)
        .post()
    } catch (e) {
      return Promise.reject(e)
    }
    return loginService.logInWithBasic(
      data.username,
      data.password,
      true
    )
  }

}

const userService = new UserService()
export default userService