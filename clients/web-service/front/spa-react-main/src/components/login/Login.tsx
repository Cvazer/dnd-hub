import React from 'react';
import LoginForm from '@comp/login/LoginForm';
import loginService from '@src/logic/LoginService';
import '@css/login/Login.less';
import { Card } from '@blueprintjs/core';
import { useTsSelector } from '@src/store/ReduxConfig';
import { Navigate } from 'react-router-dom';

type Props = Record<string, never>

const Login: React.FC<Props> = () => {
  const user = useTsSelector(store => store.account.user)

  return user === undefined ? (
    <div className="login-root">
      <Card className="login-form-container">
        <LoginForm loginService={loginService}/>
      </Card>
    </div>
  ) : (
    <Navigate to="/" replace/>
  )
}

export default Login