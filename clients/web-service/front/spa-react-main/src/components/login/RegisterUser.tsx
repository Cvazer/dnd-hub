import React from 'react';
import '@css/login/RegisterUser.less';
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { Button, Card, FormGroup, InputGroup, Intent } from '@blueprintjs/core';
import SpanLabel from '@comp/common/SpanLabel';
import userService from '@src/logic/UserService';
import toasterService from '@src/logic/ToasterService';
import { useNavigate } from 'react-router-dom';

export type IRegisterData = {
  username: string,
  password: string,
  repeatPassword: string
}

const registerDataSchema = yup.object({
  username: yup.string().required("Username must be set"),
  password: yup.string().required("Password must be set").min(8).max(254),
  repeatPassword: yup.string().oneOf([yup.ref("password")], "Passwords don't match")
}).required()

const RegisterUser: React.FC<Record<string, never>> = () => {
  const navigate = useNavigate()
  const { control, handleSubmit, formState: { errors } } = useForm<IRegisterData>({
    resolver: yupResolver(registerDataSchema),
    shouldFocusError: false,
    reValidateMode: "onBlur"
  })
  const onSubmit = (data: IRegisterData) => {
    userService.register(data)
      .catch((e) => toasterService.error(e.description))
      .then(() => {navigate("/", { replace: true })})
  }

  return (
    <div className="register-user-root">
      <Card className="register-user-form-container">
        <form onSubmit={handleSubmit(onSubmit)}>
          <FormGroup label={<SpanLabel text="Username"/>}
                     labelInfo={errors.username?.message
                       && <SpanLabel text={errors.username?.message}
                                     intent={Intent.DANGER}/>}>
            <Controller name="username"
                        defaultValue={""}
                        control={control}
                        render={({field}) => (
                          <InputGroup {...field}/>
                        )}/>
          </FormGroup>
          <FormGroup label={<SpanLabel text="Password"/>}
                     labelInfo={errors.password?.message
                       && <SpanLabel text={errors.password?.message}
                                     intent={Intent.DANGER}/>}>
            <Controller name="password"
                        defaultValue={""}
                        control={control}
                        render={({field}) => (
                          <InputGroup type="password" {...field}/>
                        )}/>
          </FormGroup>
          <FormGroup label={<SpanLabel text="Repeat password"/>}
                     labelInfo={errors.repeatPassword?.message
                       && <SpanLabel text={errors.repeatPassword?.message}
                                     intent={Intent.DANGER}/>}>
            <Controller name="repeatPassword"
                        defaultValue={""}
                        control={control}
                        render={({field}) => (
                          <InputGroup type="password" {...field}/>
                        )}/>
          </FormGroup>
          <br/>
          <Button text="Create user" fill type="submit"
                  intent={Intent.PRIMARY}/>
        </form>
      </Card>
    </div>
  )
}

export default RegisterUser