import React, { KeyboardEvent, KeyboardEventHandler, useRef, useState } from 'react';
import '@css/login/LoginForm.less';
import { ILoginService } from '@src/logic/LoginService';
import { Link, useNavigate } from 'react-router-dom';
import { Button, Checkbox, InputGroup, Intent, Label } from '@blueprintjs/core';
import GoogleLogo from '@assets/images/google.svg';
import { ErrorInfo } from '@src/logic/backend/BackendService';
import toasterService from '@src/logic/ToasterService';

type Props = { loginService: ILoginService }

const LoginForm: React.FC<Props> = ({loginService }) => {

  const [username, setUsername] = useState<string>("")
  const [password, setPassword] = useState<string>("")
  const [inputIntent, setInputIntent] = useState<Intent>(Intent.NONE)

  const passRef = useRef<HTMLInputElement>()
  const btnRef = useRef<HTMLButtonElement>()
  const rememberMeRef = useRef<HTMLInputElement>()

  const navigate = useNavigate()

  return (
    <form className="login-form-root-container" autoSave="on"
          onSubmit={e => {e.preventDefault()}}>
      <div className="username-block">
        <Label>Username:
          <InputGroup onChange={e => {
                        setUsername(e.target.value)
                        setInputIntent(Intent.NONE)
                      }} type="text" value={username} name="username"
                      intent={inputIntent}
                      onKeyDown={doIfKey("Enter", () => {
                        passRef.current.focus()
                      })}/>
        </Label>
      </div>
      <div className="password-block">
        <Label>Password:
          <InputGroup onChange={e => {
                        setPassword(e.target.value)
                        setInputIntent(Intent.NONE)
                      }} fill intent={inputIntent}
                      rightElement={
                        <Button minimal icon="eye-open" onClick={() => {
                          passRef.current.type = "text"
                          passRef.current.focus()
                        }}/>
                      }
                      type="password" name="password" value={password}
                      onBlur={e => e.target.type = "password"}
                      onKeyDown={doIfKey("Enter", () => {
                        passRef.current.blur()
                        btnRef.current.click()
                      })}
                      inputRef={passRef}/>
        </Label>
      </div>
      <div className="remember-me-block">
        <div><Checkbox label={"Remember me"} defaultChecked inputRef={rememberMeRef}/></div>
        <div><Link to={"/login/register"}>Need new account?</Link></div>
      </div>
      <div className="login-block">
        <Button elementRef={btnRef} text={"Log In"}
                intent={Intent.PRIMARY}
                onClick={() => {
                  loginService.logInWithBasic(
                    username, password, rememberMeRef.current.checked
                  ).then(() => navigate("/"))
                    .catch((e: ErrorInfo) => {
                      setInputIntent(Intent.DANGER)
                      toasterService.error(e.description)
                    })
        }}/>
        <Button
          text={"Login with Google"} outlined
          rightIcon={<GoogleLogo/>}
          onClick={() => {
            loginService.getGoogleLoginUrl()
              .then((res: string) => {window.location.href = res})
        }}/>
      </div>
    </form>
  )
}

function doIfKey<T>(key: string, action: () => void): KeyboardEventHandler<T> {
  return (event: KeyboardEvent<T>) => event.key === key && action()
}

export default LoginForm