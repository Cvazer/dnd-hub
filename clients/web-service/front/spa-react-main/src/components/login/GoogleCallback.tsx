import React from 'react';
import { useNavigate, useSearchParams } from 'react-router-dom';
import toasterService from '@src/logic/ToasterService';
import { useEffectOnce } from '@src/misc/useEffectOnce';
import loginService from '@src/logic/LoginService';

type Props = Record<string, never>

const GoogleCallback: React.FC<Props> = () => {
  const [searchParams] = useSearchParams();
  const navigate = useNavigate();

  useEffectOnce(() => {
    const code: string = searchParams.get("code");
    code && loginService.logInWithGoogleCode(code)
      .then(() => navigate("/"))
      .catch(e => {
        toasterService.error(e.description)
        navigate("/login")
      })
  })
  return <div/>
}

export default GoogleCallback