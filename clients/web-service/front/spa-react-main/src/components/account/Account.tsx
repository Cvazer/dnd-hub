import React, { Fragment } from 'react';
import AccountSettings from '@comp/account/settings/AccountSettings';
import { Container } from 'react-grid-system';

type Props = Record<string, never>

const Account: React.FC<Props> = () => {
  return (
    <Fragment>
      <Container>
        <AccountSettings />
      </Container>
    </Fragment>
  )
}

export default Account