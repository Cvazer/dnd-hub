import React, { Fragment } from 'react';
import { Button, Card, Intent } from '@blueprintjs/core';
import { Col, Row } from 'react-grid-system';
import GoogleLogo from '@assets/images/google.svg';
import { SegmentProps } from '@comp/account/settings/credentials/CredentialsSegmentFactory';
import '@css/account/settings/CredentialsSegment.less';
import cn from 'classnames';
import { Callout } from '@blueprintjs/core/lib/esnext';

const GoogleCredentialsSegment: React.FC<SegmentProps> = (
  {provider, exists}
) => {
  return (
    <Card className={cn(
      "google-credentials-segment",
      "credentials-segment"
    )} key={provider}>
      { exists
        ? <Connected/>
        : <Disconnected/>
      }
    </Card>
  )
}

const Connected: React.FC<Record<string, never>> = () => {
  return (
    <Fragment>
      <div>
        <Callout intent={Intent.PRIMARY}>
          Google account is now connected! You can us it to log in
        </Callout>
      </div>
      <div>
        <Button text="Disconnect" outlined large
                icon={<GoogleLogo/>}
                intent={Intent.DANGER}/>
      </div>
    </Fragment>
  )
}

const Disconnected: React.FC<Record<string, never>> = () => {
  return (
    <Fragment>
      <Row>
        <Col className="text">
          Google account is not connected! If you wish to log in to this
          account via Google you must link it
        </Col>
      </Row>
      <Row>
        <Col className="button">
          <Button text="Connect" outlined large
                  icon={<GoogleLogo/>}
                  intent={Intent.PRIMARY}/>
        </Col>
      </Row>
    </Fragment>
  )
}

export default GoogleCredentialsSegment