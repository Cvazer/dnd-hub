import React from 'react';
import BasicCredentialsSegment from '@comp/account/settings/credentials/BasicCredentialsSegment';
import GoogleCredentialsSegment from '@comp/account/settings/credentials/GoogleCredentialsSegment';

export type UserCredentialsDto = {
  provider: CredentialProvider,
  exists: boolean
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  data: any
}

export type SegmentProps = UserCredentialsDto & {
  updateListCallback: () => void
}

export enum CredentialProvider {
  GOOGLE = "GOOGLE",
  BASIC = "BASIC"
}

export interface ICredentialsSegmentFactory {
  create(dto: UserCredentialsDto, updateList: () => void): JSX.Element
}

class CredentialsSegmentFactory implements ICredentialsSegmentFactory {
  create(dto: UserCredentialsDto, updateList: () => void): JSX.Element {
    if (dto.provider === CredentialProvider.BASIC) {
      return <BasicCredentialsSegment data={dto.data}
                                      key={dto.provider}
                                      exists={dto.exists}
                                      updateListCallback={updateList}
                                      provider={dto.provider}/>
    } else if (dto.provider === CredentialProvider.GOOGLE) {
      return <GoogleCredentialsSegment data={dto.data}
                                       key={dto.provider}
                                       exists={dto.exists}
                                       updateListCallback={updateList}
                                       provider={dto.provider}/>
    } else {
      throw `Unknown provider "${dto.provider}"`
    }
  }
}

const credentialsSegmentFactory = new CredentialsSegmentFactory()
export default credentialsSegmentFactory