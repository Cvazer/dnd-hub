import React, { Fragment, useState } from 'react';
import { Button, Callout, Card, FormGroup, InputGroup, Intent } from '@blueprintjs/core';
import { Col, Row } from 'react-grid-system';
import cn from 'classnames';
import { SegmentProps } from '@comp/account/settings/credentials/CredentialsSegmentFactory';
import '@css/account/settings/CredentialsSegment.less';
import toasterService from '@src/logic/ToasterService';
import credentialsService from '@src/logic/CredentialsService';

const BasicCredentialsSegment: React.FC<SegmentProps> = (
  {updateListCallback, exists, data}
) => {

  return (
    <Card className={cn("basic-credentials-segment", "credentials-segment")}>
      {
        exists
          ? <SetCredentials data={data} updateListCallback={updateListCallback}/>
          : <NonSetCredentials updateListCallback={updateListCallback}/>
      }
    </Card>
  )
}

type SetCredentialsProps = {
  // eslint-disable-next-line
  data: any,
  updateListCallback: () => void
}

const SetCredentials: React.FC<SetCredentialsProps> = ({data, updateListCallback}) => {
  const [username, setUsername] = useState<string>(data.username)
  const [password, setPassword] = useState<string>("")
  const [oldPassword, setOldPassword] = useState<string>("")

  const clean = () => {
    setUsername(data.username)
    setPassword("")
    setOldPassword("")
  }

  return (
    <Fragment>
      <Callout>
        Here you can change your basic credentials such as username
        and password
      </Callout>
      <br/>
      <Row>
        <Col>
          <FormGroup label={
            <span className={cn("bp4-text-small", "bp4-text-muted")}>
              Username:
            </span>
          }>
            <InputGroup value={username} small
                        autoComplete="off"
                        onChange={e => setUsername(e.target.value)}
            />
          </FormGroup>
        </Col>
        <Col>
          <FormGroup label={
            <span className={cn("bp4-text-small", "bp4-text-muted")}>
              New password:
            </span>
          }>
            <InputGroup value={password} small type="password"
                        autoComplete="off"
                        onChange={e => setPassword(e.target.value)}
            />
          </FormGroup>
        </Col>
      </Row>
      <Row>
        <Col>
          <FormGroup label={
            <span className={cn("bp4-text-small", "bp4-text-muted")}>
              Current password:
            </span>
          } helperText={
            <span>
              In order to change your basic credentials you need to
              provide your current password
            </span>
          }>
            <InputGroup small type="password" autoComplete="off"
                        value={oldPassword}
                        onChange={e => setOldPassword(e.target.value)}/>
          </FormGroup>
        </Col>
        <Col>
          <br/>
          <Button text={"Confirm"} fill
                  onClick={e => {
                    e.preventDefault()
                    credentialsService.setBasic(
                      oldPassword, username, password
                    ).then(() => updateListCallback())
                      .then(() => clean())
                      .catch(e => toasterService.error(e.description))
                  }} intent={Intent.SUCCESS}/>
        </Col>
      </Row>
    </Fragment>
  )
}

type NonSetCredentialsProps = {
  updateListCallback: () => void
}

const NonSetCredentials: React.FC<NonSetCredentialsProps> = (
  {updateListCallback}
) => {
  const [username, setUsername] = useState<string>("")
  const [password, setPassword] = useState<string>("")

  return (
    <Fragment>
      <Row>
        <Col className="left-section">
          In order to login into this account using username
          and password you need to set them up
        </Col>
        <Col>
          <FormGroup label={
            <span className={cn("bp4-text-small", "bp4-text-muted")}>
                  Username:
                </span>
          }>
            <InputGroup type="text" small
                        onChange={e => setUsername(e.target.value)}
                        value={username}
            />
          </FormGroup>
          <FormGroup label={
            <span className={cn("bp4-text-small", "bp4-text-muted")}>
                  Password:
                </span>
          }>
            <InputGroup type="password" small
                        onChange={e => setPassword(e.target.value)}
                        value={password}
            />
          </FormGroup>
          <Button text={"Set credentials"} fill
                  intent={Intent.PRIMARY} onClick={e => {
            e.preventDefault()
            credentialsService.createBasic(username, password)
              .then(() => updateListCallback())
              .catch(e => toasterService.error(e.description))
          }}/>
        </Col>
      </Row>
    </Fragment>
  )
}

export default BasicCredentialsSegment