import React, { useEffect } from 'react';
import { Callout } from '@blueprintjs/core';
import { Col, Row } from 'react-grid-system';
import { BackendRequestBuilder } from '@src/logic/backend/BackendRequestBuilder';
import { BackendEndpoint } from '@src/logic/backend/BackendEndpoints';
import credentialsSegmentFactory, {
  UserCredentialsDto,
} from '@comp/account/settings/credentials/CredentialsSegmentFactory';
import '@css/account/settings/AccountSettings.less';
import { User } from '@src/store/account/User';
import { useQuery } from 'react-query';
import { useTsSelector } from '@src/store/ReduxConfig';

type Props = Record<string, never>
type SegmentProps = {
  user: User
}

const AccountSettings: React.FC<Props> = () => {
  const user = useTsSelector(state => state.account.user)
  return (
    <div className="account-settings-root">
      <CredentialsSegment user={user}/>
    </div>
  )
}

const CredentialsSegment: React.FC<SegmentProps> = ({user}) => {
  const {data: list, refetch, isLoading} = useQuery(["get-credentials-list"], () => {
    return new BackendRequestBuilder()
      .endpoint(BackendEndpoint.API_CORE_CREDENTIALS)
      .get<Array<UserCredentialsDto>>()
  })

  useEffect(() => {refetch()}, [user])

  return (
    <div className="credentials-root">
      <h3>Credentials</h3>
      <Callout>
        All of your linked credentials are visible here. You can use any of
        them to log into your account. Also you can unlink old ones and link
        new credentials.
      </Callout>
      <br/>
      <div className="credentials-segment-root">
        <Row className="credentials-segment-container">
          {!isLoading && list.map(dto => (
            <Col xs={12} md={6} key={dto.provider}>
              {credentialsSegmentFactory.create(dto, refetch)}
            </Col>
          ))}
        </Row>
      </div>
    </div>
  )
}

export default AccountSettings