import React, { Fragment } from 'react';
import { BackendRequestBuilder } from '@src/logic/backend/BackendRequestBuilder';
import { BackendEndpoint } from '@src/logic/backend/BackendEndpoints';
import { useEffectOnce } from '@src/misc/useEffectOnce';

type Props = Record<string, never>

const CheckAlive: React.FC<Props> = () => {

  useEffectOnce(() => {
    new BackendRequestBuilder()
      .endpoint(BackendEndpoint.CORE_CHECK)
      .get()
      .then(res => console.log(res))
      .catch(e => console.log("Error in check: ", e))
    return () => {};
  })
  return (<Fragment/>)
}

export default CheckAlive