import React, { PropsWithChildren } from 'react';
import cn from 'classnames';
import { Intent } from '@blueprintjs/core';

type Props = {
  text?: string,
  intent?: Intent
}

const SpanLabel: React.FC<PropsWithChildren<Props>> = (
  {text, children, intent = Intent.NONE}
) => {
  const getClassFromIntent = (intent: Intent): string => {
    switch (intent) {
      case 'danger': return "bp4-intent-danger"
      case 'none': return ""
      case 'primary': return "bp4-intent-primary"
      case 'success': return "bp4-intent-success"
      case 'warning': return "bp4-intent-warning"
    }
  }

  return (
    <span className={cn(
      "bp4-text-small",
      "bp4-text-muted",
      intent && intent !== Intent.NONE && "bp4-tag",
      intent && intent !== Intent.NONE && getClassFromIntent(intent)
    )}>
      { text ? text : children}
    </span>
  )
}

export default SpanLabel