import React, { Fragment } from 'react';
import '@css/Application.less';
import MainHeader from '@comp/header/MainHeader';
import loginService from '@src/logic/LoginService';
import { Route, Routes, useLocation, useNavigate } from 'react-router-dom';
import GoogleCallback from '@comp/login/GoogleCallback';
import { useTsSelector } from '@src/store/ReduxConfig';
import Dashboard from '@comp/dashboard/Dashboard';
import Login from '@comp/login/Login';
import Account from '@comp/account/Account';
import toasterService from '@src/logic/ToasterService';
import { useEffectOnce } from '@src/misc/useEffectOnce';
import RegisterUser from '@comp/login/RegisterUser';


type Props = Record<string, never>

const Application: React.FC<Props> = () => {

  const token = useTsSelector(state => state.account.token)
  const user = useTsSelector(state => state.account.user)
  const navigate = useNavigate()
  const location = useLocation()

  useEffectOnce(() => {
    if (token !== null && user !== null) return
    const savedToken = localStorage.getItem("jwt")
    if (savedToken === null || savedToken === "null") {
      if (location.pathname !== "/login/google") {
        navigate("/login", { replace: true })
      }
      return;
    }
    console.log(savedToken === null || savedToken === "null");
    loginService.logInWithOldToken(savedToken)
      .catch(e => {
        toasterService.error(e.description);
        navigate("/login", { replace: true })
      })
  })

  if (user === undefined) {
    return (
      <Fragment>
        <Routes>
          <Route path="/">
            <Route index element={<div/>}/>
            <Route path="login">
              <Route index element={<Login/>}/>
              <Route path="google" element={<GoogleCallback/>}/>
              <Route path="register" element={<RegisterUser/>}/>
            </Route>
          </Route>
        </Routes>
      </Fragment>
    )
  } else {
    return (
      <Fragment>
        <MainHeader />
        <Routes>
          <Route path="/">
            <Route index element={<Dashboard/>}/>
            <Route path="account" element={<Account/>}/>
          </Route>
        </Routes>
      </Fragment>
    )
  }
};



export default Application;
