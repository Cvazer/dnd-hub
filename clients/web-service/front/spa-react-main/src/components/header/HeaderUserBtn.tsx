import React, { Fragment } from 'react';
import { Button, Divider, Intent, Tag } from '@blueprintjs/core';
import { Classes } from '@blueprintjs/popover2';
import '@css/header/HeaderUserBtn.less';
import userService from '@src/logic/UserService';
import { useTsSelector } from '@src/store/ReduxConfig';
import { useNavigate } from 'react-router-dom';
import { useToggle } from '@src/misc/useToggle';

type Props = Record<string, never>

const HeaderUserBtn: React.FC<Props> = () => {
  const [open, toggle] = useToggle()

  return <div className="header-user">
    <Button icon="user" minimal onClick={toggle}/>
    <div className="content"
         style={{display: open ? "block" : "none"}}>
      <Content toggle={toggle}/>
    </div>
  </div>
}

type ContentProps = {
  toggle: () => void
}

const Content: React.FC<ContentProps> = ({toggle}) => {
  const user = useTsSelector(state => state.account.user)
  const navigate = useNavigate()

  return <Fragment>
    <div>
      <div className="nickname">
        <Tag icon="user" minimal large fill>{user.nickname}</Tag>
      </div>
      <Divider/>
      <Button minimal fill
              alignText="left"
              className={Classes.POPOVER2_DISMISS}
              text={"Settings"}
              icon="cog" onClick={() => {
                toggle();
                navigate("/account")
              }}/>
      <Divider/>
      <Button minimal fill
              alignText="left"
              icon="log-out"
              text={"Log out"}
              className={Classes.POPOVER2_DISMISS}
              intent={Intent.DANGER} onClick={() => {
                toggle();
                userService.logOut()
                  .then(() => navigate("/login"))
              }}/>
    </div>
  </Fragment>
}
export default HeaderUserBtn