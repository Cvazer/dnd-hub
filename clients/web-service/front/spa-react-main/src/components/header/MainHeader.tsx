import React from 'react';
import '@css/header/MainHeader.less';
import { Button } from '@blueprintjs/core';
// import Logo from '@assets/images/dnd.svg';
import HeaderUserBtn from '@comp/header/HeaderUserBtn';

type Props = Record<string, never>

const MainHeader: React.FC<Props> = () => {
  return (
    <div className="header">
      <div className="left-section">
        <div style={{padding: "0 5px"}}><Button icon="menu" minimal/></div>
      </div>
      <div className="central-section">
        <div>Search and etc.</div>
      </div>
      <div className="right-section">
        <Button icon="notifications" minimal/>
        <HeaderUserBtn />
      </div>
    </div>
  )
}

export default MainHeader