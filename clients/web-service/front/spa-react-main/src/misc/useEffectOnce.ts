import { EffectCallback, useEffect, useRef } from 'react';

export const useEffectOnce = (effect: EffectCallback) => {
  const executed = useRef<boolean>(false);

  useEffect(() => {
    if (executed.current) return
    executed.current = true
    return effect()
  }, [])
}