#!/bin/bash
cd ../../../../ || exit
echo cleaning microservices ...
./gradlew clients:web-service:microservices:core:clean
./gradlew clients:web-service:microservices:runner:clean
echo creating bootable jars for microservices ...
./gradlew clients:web-service:microservices:core:bootJar
./gradlew clients:web-service:microservices:runner:bootJar