#!/bin/bash
cd ../docker || exit
docker compose rm -v
cd ../scripts || exit
./build-microservices.sh
cd ../docker || exit
docker compose up