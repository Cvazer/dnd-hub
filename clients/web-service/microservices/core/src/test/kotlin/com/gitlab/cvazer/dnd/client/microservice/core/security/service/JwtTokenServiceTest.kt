package com.gitlab.cvazer.dnd.client.microservice.core.security.service

import com.gitlab.cvazer.dnd.client.microservice.core.security.RsaKeyProvider
import com.gitlab.cvazer.dnd.client.microservice.core.security.config.CoreSecurityProperties
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator
import com.nimbusds.jose.proc.BadJWSException
import com.nimbusds.jwt.proc.BadJWTException
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.junit.jupiter.MockitoExtension
import org.mockito.junit.jupiter.MockitoSettings
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import org.mockito.quality.Strictness

@ExtendWith(MockitoExtension::class)
@MockitoSettings(strictness = Strictness.LENIENT)
internal class JwtTokenServiceTest {

    @Test
    @DisplayName("validate() token timeout")
    fun validateTokenTimeout() {
        val props = CoreSecurityProperties(token = CoreSecurityProperties.Token(-100))
        val service = JwtTokenService(props, RsaKeyProvider(props), mock())

        val token = assertDoesNotThrow {
            service.generate("TEST_USER", "basic", false)
        }.data!!

        assertThrows<BadJWTException> { service.validate(token) }
    }

    @Test
    @DisplayName("validate() invalid signature")
    fun validateSignatureCheck() {
        var keys = RSAKeyGenerator(2048).generate()
        val providerMock = mock<RsaKeyProvider>()

        doReturn(keys).whenever(providerMock).privateRSAKey
        doReturn(keys.toPublicJWK()).whenever(providerMock).publicRSAKey

        var service = JwtTokenService(CoreSecurityProperties(), providerMock, mock())

        val token = assertDoesNotThrow {
            service.generate("TEST_USER", "basic", false)
        }.data!!

        keys = RSAKeyGenerator(2048).generate()
        doReturn(keys).whenever(providerMock).privateRSAKey
        doReturn(keys.toPublicJWK()).whenever(providerMock).publicRSAKey
        service = JwtTokenService(CoreSecurityProperties(), providerMock, mock())

        assertThrows<BadJWSException> { service.validate(token) }
    }
}