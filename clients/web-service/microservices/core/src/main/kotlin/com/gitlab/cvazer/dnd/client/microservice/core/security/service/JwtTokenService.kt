package com.gitlab.cvazer.dnd.client.microservice.core.security.service

import com.gitlab.cvazer.dnd.client.microservice.core.dao.repo.UserRepo
import com.gitlab.cvazer.dnd.client.microservice.core.security.RsaKeyProvider
import com.gitlab.cvazer.dnd.client.microservice.core.security.config.CoreSecurityProperties
import com.gitlab.cvazer.dnd.client.microservice.core.security.model.ClaimsPersistingCoreAuthentication
import com.gitlam.cvazer.dnd.client.microservice.runner.security.TokenService
import com.gitlam.cvazer.dnd.client.microservice.runner.serviceResponse
import com.gitlam.cvazer.dnd.client.microservice.runner.web.comms.ErrorInfo
import com.gitlam.cvazer.dnd.client.microservice.runner.web.comms.ServiceResponse
import com.nimbusds.jose.JOSEObjectType
import com.nimbusds.jose.JWSAlgorithm
import com.nimbusds.jose.JWSHeader
import com.nimbusds.jose.JWSObject
import com.nimbusds.jose.crypto.RSASSASigner
import com.nimbusds.jose.crypto.RSASSAVerifier
import com.nimbusds.jose.jwk.JWK
import com.nimbusds.jose.jwk.JWKSet
import com.nimbusds.jose.jwk.source.ImmutableJWKSet
import com.nimbusds.jose.proc.BadJWSException
import com.nimbusds.jose.proc.JWSVerificationKeySelector
import com.nimbusds.jose.proc.SecurityContext
import com.nimbusds.jwt.JWTClaimsSet
import com.nimbusds.jwt.SignedJWT
import com.nimbusds.jwt.proc.BadJWTException
import com.nimbusds.jwt.proc.DefaultJWTClaimsVerifier
import com.nimbusds.jwt.proc.DefaultJWTProcessor
import org.apache.commons.lang3.time.DateUtils
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Service
import java.util.*

@Service
class JwtTokenService(
    private val coreSecurityProperties: CoreSecurityProperties,
    private val rsaKeyProvider: RsaKeyProvider,
    private val userRepo: UserRepo
): TokenService {
    override fun generate(subject: String, provider: String, refreshable: Boolean): ServiceResponse<String> = Date().let { now ->
        SignedJWT(
            JWSHeader.Builder(JWSAlgorithm.RS256)
                .keyID(rsaKeyProvider.privateRSAKey.keyID)
                .type(JOSEObjectType.JWT)
                .build(),
            JWTClaimsSet.Builder()
                .subject(subject)
                .audience(coreSecurityProperties.token.audience)
                .expirationTime(DateUtils.addSeconds(now, coreSecurityProperties.token.lifetimeInSeconds))
                .jwtID(UUID.randomUUID().toString())
                .claim("prv", provider)
                .claim("ref", refreshable.toString())
                .issueTime(now)
                .build()
        ).also { it.sign(RSASSASigner(rsaKeyProvider.privateRSAKey)) }.serialize().serviceResponse()
    }

    override fun checkSignature(token: String): ServiceResponse<Boolean> = JWSObject
        .parse(token)
        .verify(RSASSAVerifier(rsaKeyProvider.publicRSAKey))
        .serviceResponse()

    private fun createJwtProcessor(): DefaultJWTProcessor<SecurityContext> =
        DefaultJWTProcessor<SecurityContext>()
//            .apply { setJWSTypeVerifier(DefaultJOSEObjectTypeVerifier(JOSEObjectType.JWT)) }
            .apply {
                setJWSKeySelector(
                    JWSVerificationKeySelector(
                        JWSAlgorithm.RS256,
                        ImmutableJWKSet(JWKSet(mutableListOf(
                            rsaKeyProvider.publicRSAKey as JWK,
                            rsaKeyProvider.privateRSAKey as JWK
                        )))
                    )
                )
            }

    @Throws(
        BadJWTException::class,
        BadJWSException::class,
        BadCredentialsException::class)
    override fun validate(token: String): ServiceResponse<Authentication> {
        val claims = createJwtProcessor()
            .apply { setJWTClaimsSetVerifier(DefaultJWTClaimsVerifier(
                JWTClaimsSet.Builder()
                    .audience(coreSecurityProperties.token.audience)
                    .build(),
                setOf("exp")
            )) }.process(token, null)
        val user = userRepo.findById(claims.subject.toLong()).orElse(null)
            ?: throw BadCredentialsException("No user with id ${claims.subject}")
        return ClaimsPersistingCoreAuthentication(user, token, claims)
            .also { it.isAuthenticated = true }
            .serviceResponse()
    }

    fun parseWithoutValidatingClaims(token: String): ServiceResponse<JWTClaimsSet> = SignedJWT.parse(token)
        .takeIf { it.verify(RSASSAVerifier(rsaKeyProvider.publicRSAKey)) }
        ?.jwtClaimsSet?.serviceResponse()
        ?: ErrorInfo("Invalid JWT signature!").serviceResponse()
}