package com.gitlab.cvazer.dnd.client.microservice.core.security.config

import com.gitlam.cvazer.dnd.client.microservice.runner.security.FilterChainMixin
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configurers.AuthorizeHttpRequestsConfigurer
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder

@Configuration
class CoreSecurityConfig: FilterChainMixin {

    @Bean
    fun passwordEncoder(): PasswordEncoder = BCryptPasswordEncoder()

    override fun mix(http: AuthorizeHttpRequestsConfigurer<HttpSecurity>.AuthorizationManagerRequestMatcherRegistry) {
        http
            .requestMatchers(HttpMethod.PUT,"/api/core/**").hasAuthority("USER")
            .requestMatchers(HttpMethod.POST,"/api/core/**").hasAuthority("USER")
            .requestMatchers(HttpMethod.GET,"/api/core/**").hasAuthority("USER")
            .requestMatchers(HttpMethod.DELETE,"/api/core/**").hasAuthority("USER")
            .requestMatchers("/core/login/**", "/core/check").permitAll()
    }

}