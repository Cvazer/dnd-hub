package com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.model

class CredentialsCreationContext private constructor(
    val provider: CredentialsProvider,
    val username: String,
    val password: String? = null
) {
    companion object {
        @JvmStatic
        fun createForGoogle(username: String): CredentialsCreationContext {
            return CredentialsCreationContext(CredentialsProvider.GOOGLE, username)
        }

        @JvmStatic
        fun createForBasic(username: String, password: String): CredentialsCreationContext {
            return CredentialsCreationContext(CredentialsProvider.BASIC, username, password)
        }
    }
}