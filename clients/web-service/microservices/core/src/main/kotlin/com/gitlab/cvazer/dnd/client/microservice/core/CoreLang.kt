package com.gitlab.cvazer.dnd.client.microservice.core

import com.gitlab.cvazer.dnd.core.model.auth.AuthResult
import com.gitlab.cvazer.dnd.core.model.user.User
import com.gitlam.cvazer.dnd.client.microservice.runner.responseEntity
import com.gitlam.cvazer.dnd.client.microservice.runner.serviceResponse
import com.gitlam.cvazer.dnd.client.microservice.runner.web.comms.ErrorInfo
import com.gitlam.cvazer.dnd.client.microservice.runner.web.comms.ServiceResponse
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

fun <D> AuthResult<D>.serviceResponse(): ServiceResponse<Pair<D, User>> =
    when (this.status) {
        AuthResult.Status.OK -> Pair(this.data, this.user!!).serviceResponse()
        AuthResult.Status.ERROR -> ErrorInfo(this.error!!).serviceResponse()
    }

fun <T> ServiceResponse<T>.trueOrError(error: ErrorInfo, predicate: (data: T) -> Boolean): ServiceResponse<T> =
    if (this.isFailure()) this
    else if (predicate.invoke(this.data!!)) this
    else error.serviceResponse()

fun <T> ServiceResponse<T>.responseEntity(failStatus: HttpStatus): ResponseEntity<ServiceResponse<T>> =
    if (this.isFailure()) ResponseEntity(this, failStatus)
    else this.responseEntity()