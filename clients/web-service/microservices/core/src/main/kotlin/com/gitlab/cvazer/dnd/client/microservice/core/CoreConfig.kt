package com.gitlab.cvazer.dnd.client.microservice.core

import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@ComponentScan
@Configuration
@ConfigurationPropertiesScan
@EnableJpaRepositories(basePackageClasses = [CoreConfig::class])
@EntityScan(basePackageClasses = [CoreConfig::class])
@PropertySource(
    value = ["classpath:core.properties", "file:core.properties"],
    ignoreResourceNotFound = true
)
class CoreConfig