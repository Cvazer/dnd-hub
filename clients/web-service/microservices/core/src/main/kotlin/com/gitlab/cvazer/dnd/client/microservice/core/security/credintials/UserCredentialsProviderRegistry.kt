package com.gitlab.cvazer.dnd.client.microservice.core.security.credintials

import com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.model.CredentialsProvider
import com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.model.UserCredentials
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class UserCredentialsProviderRegistry(
    private val providers: Map<CredentialsProvider, UserCredentialsProvider<*, *>>
) {
    @Autowired constructor(providers: List<UserCredentialsProvider<*, *>>): this(
        providers.associateBy { it.provider }
    )

    @Suppress("UNCHECKED_CAST")
    operator fun <T: UserCredentialsProvider<*, *>> get(provider: CredentialsProvider): T? {
        return providers[provider] as T
    }

    @Suppress("UNCHECKED_CAST")
    fun getAll(): List<UserCredentialsProvider<Any, UserCredentials>> {
        return providers.values.toList()
                as List<UserCredentialsProvider<Any, UserCredentials>>
    }
}