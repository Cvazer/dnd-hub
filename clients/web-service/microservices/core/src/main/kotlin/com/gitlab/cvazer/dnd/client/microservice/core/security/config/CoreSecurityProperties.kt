package com.gitlab.cvazer.dnd.client.microservice.core.security.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.NestedConfigurationProperty
import org.springframework.boot.context.properties.bind.ConstructorBinding


@ConfigurationProperties("core.security")
open class CoreSecurityProperties {
    @NestedConfigurationProperty val keys: Keys
    @NestedConfigurationProperty val token: Token
    @NestedConfigurationProperty val google: Google

    @Suppress("ConvertSecondaryConstructorToPrimary")
    @ConstructorBinding
    constructor(keys: Keys = Keys(), token: Token = Token(), google: Google = Google()) {
        this.keys = keys
        this.token = token
        this.google = google
    }

    class Keys {
        val pkcs8PrivateFilePath: String
        val pkcs8PublicFilePath: String

        @Suppress("ConvertSecondaryConstructorToPrimary")
        @ConstructorBinding
        constructor(
            pkcs8PrivateFilePath: String = "rsa-pkcs8-private",
            pkcs8PublicFilePath: String = "rsa-pkcs8-public"
        ) {
            this.pkcs8PrivateFilePath = pkcs8PrivateFilePath
            this.pkcs8PublicFilePath = pkcs8PublicFilePath
        }
    }

    class Token {
        val lifetimeInSeconds: Int
        val audience: String

        @Suppress("ConvertSecondaryConstructorToPrimary")
        @ConstructorBinding
        constructor(
            lifetimeInSeconds: Int = 900,
            audience: String = "dnd-hub.front"
        ) {
            this.lifetimeInSeconds = lifetimeInSeconds
            this.audience = audience
        }
    }

    class Google {
        val clientId: String
        val clientSecret: String
        val scope: String
        val redirectUri: String

        @Suppress("ConvertSecondaryConstructorToPrimary")
        @ConstructorBinding
        constructor(
            clientId: String = "NULL",
            clientSecret: String = "NULL",
            scope: String = "openid profile email",
            redirectUri: String = "NULL"
        ) {
            this.clientId = clientId
            this.clientSecret = clientSecret
            this.scope = scope
            this.redirectUri = redirectUri
        }
    }
}