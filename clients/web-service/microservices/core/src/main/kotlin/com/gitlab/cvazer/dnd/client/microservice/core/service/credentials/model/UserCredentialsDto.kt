package com.gitlab.cvazer.dnd.client.microservice.core.service.credentials.model

import com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.model.CredentialsProvider

data class UserCredentialsDto(
    val provider: CredentialsProvider,
    val exists: Boolean = false,
    val data: Map<String, Any?> = mapOf()
)