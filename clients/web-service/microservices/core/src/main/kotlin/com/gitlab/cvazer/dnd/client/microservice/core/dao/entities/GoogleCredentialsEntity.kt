package com.gitlab.cvazer.dnd.client.microservice.core.dao.entities

import com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.model.UserCredentials
import jakarta.persistence.*
import jakarta.persistence.CascadeType.REFRESH
import jakarta.persistence.CascadeType.MERGE
import jakarta.persistence.CascadeType.PERSIST
import jakarta.persistence.FetchType.EAGER

@Entity(name = "GoogleCredentials")
@Table(name = "google_credentials", schema = "core")
class GoogleCredentialsEntity(
    override val username: String,

    @Id
    @Column(name = "user_id")
    private var _userId: Long? = null,

    @MapsId
    @ManyToOne(fetch = EAGER, cascade = [PERSIST, MERGE, REFRESH], optional = false)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    override val user: UserEntity
): UserCredentials