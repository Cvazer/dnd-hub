package com.gitlab.cvazer.dnd.client.microservice.core.dao

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.bind.ConstructorBinding

@ConfigurationProperties("core.hibernate")
open class CoreHibernateProperties {
    val ddlAuto: String
    val dialect: String?

    @Suppress("ConvertSecondaryConstructorToPrimary")
    @ConstructorBinding
    constructor(ddlAuto: String = "validate", dialect: String? = null) {
        this.ddlAuto = ddlAuto
        this.dialect = dialect
    }
}