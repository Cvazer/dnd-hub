package com.gitlab.cvazer.dnd.client.microservice.core.dao

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.bind.ConstructorBinding

@ConfigurationProperties("core.datasource")
open class CoreDatasourceProperties @ConstructorBinding constructor(
    val url: String,
    val username: String,
    val password: String,
)