package com.gitlab.cvazer.dnd.client.microservice.core.service.user.model

data class UserCreationContext(
    val nickname: String
)