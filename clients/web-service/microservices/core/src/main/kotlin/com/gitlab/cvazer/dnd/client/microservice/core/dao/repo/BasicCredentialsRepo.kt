package com.gitlab.cvazer.dnd.client.microservice.core.dao.repo

import com.gitlab.cvazer.dnd.client.microservice.core.dao.entities.BasicCredentialsEntity
import com.gitlab.cvazer.dnd.client.microservice.core.dao.entities.UserEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface BasicCredentialsRepo: JpaRepository<BasicCredentialsEntity, Long> {
    fun findByUsername(username: String): BasicCredentialsEntity?
    fun findByUser(user: UserEntity): BasicCredentialsEntity?
}