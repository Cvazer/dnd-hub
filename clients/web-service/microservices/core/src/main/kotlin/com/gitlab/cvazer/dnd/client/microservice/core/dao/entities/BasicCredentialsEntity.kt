package com.gitlab.cvazer.dnd.client.microservice.core.dao.entities

import com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.model.UserCredentials
import jakarta.persistence.*
import jakarta.persistence.CascadeType.*
import jakarta.persistence.FetchType.*

@Entity(name = "BasicCredentials")
@Table(name = "basic_credentials", schema = "core")
data class BasicCredentialsEntity(
    override var username: String,

    @Id
    @Column(name = "user_id")
    private val _userId: Long? = null,

    @Column(columnDefinition = "TEXT")
    var password: String,

    @MapsId
    @ManyToOne(fetch = EAGER, cascade = [PERSIST, MERGE, REFRESH], optional = false)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    override val user: UserEntity,
): UserCredentials