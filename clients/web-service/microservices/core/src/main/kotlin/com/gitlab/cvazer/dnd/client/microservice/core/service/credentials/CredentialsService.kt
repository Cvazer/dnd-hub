package com.gitlab.cvazer.dnd.client.microservice.core.service.credentials

import com.gitlab.cvazer.dnd.client.microservice.core.dao.entities.BasicCredentialsEntity
import com.gitlab.cvazer.dnd.client.microservice.core.dao.entities.GoogleCredentialsEntity
import com.gitlab.cvazer.dnd.client.microservice.core.dao.entities.UserEntity
import com.gitlab.cvazer.dnd.client.microservice.core.dao.repo.BasicCredentialsRepo
import com.gitlab.cvazer.dnd.client.microservice.core.dao.repo.GoogleCredentialsRepo
import com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.BasicUserCredentialsProvider
import com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.GoogleUserCredentialsProvider
import com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.model.CredentialsCreationContext
import com.gitlab.cvazer.dnd.client.microservice.core.security.service.GoogleOauthService
import com.gitlab.cvazer.dnd.client.microservice.core.service.user.UserService
import com.gitlab.cvazer.dnd.client.microservice.core.trueOrError
import com.gitlam.cvazer.dnd.client.microservice.runner.serviceResponse
import com.gitlam.cvazer.dnd.client.microservice.runner.web.comms.ErrorInfo
import com.gitlam.cvazer.dnd.client.microservice.runner.web.comms.ServiceResponse
import com.nimbusds.jwt.SignedJWT
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import java.net.URLDecoder
import java.nio.charset.Charset
import jakarta.persistence.EntityManager
import jakarta.transaction.Transactional

@Service
class CredentialsService(
    private val googleUserCredentialsProvider: GoogleUserCredentialsProvider,
    private val basicUserCredentialsProvider: BasicUserCredentialsProvider,
    private val googleOauthService: GoogleOauthService,
    private val googleCredentialsRepo: GoogleCredentialsRepo,
    private val basicCredentialsRepo: BasicCredentialsRepo,
    private val userService: UserService,
    private val passwordEncoder: PasswordEncoder,
    private val entityManager: EntityManager
) {
    fun linkGoogleAccount(user: UserEntity, userCode: String): ServiceResponse<GoogleCredentialsEntity> {
        val provider = googleUserCredentialsProvider

        if (provider.find(user) !== null)
            return ErrorInfo("Already linked").serviceResponse()

        return googleOauthService.getAuthentication(
            URLDecoder.decode(userCode, Charset.defaultCharset())
        ).flatMap {
            try {
                SignedJWT.parse(it.idToken).jwtClaimsSet.serviceResponse()
            } catch (e: Exception) {
                ErrorInfo(e).serviceResponse()
            }
        }.flatMap {
            provider.create<GoogleCredentialsEntity>(user, CredentialsCreationContext.createForGoogle(
                it.subject
            )).serviceResponse()
        }
    }

    @Transactional
    fun disconnectGoogle(user: UserEntity): ServiceResponse<Unit> {
        val provider = googleUserCredentialsProvider

        return userService.getAllCredentialsForUser(user)
            .trueOrError(ErrorInfo("At least one credentials set must be set")) {
                it.size <= 1
            }.flatMap {
                provider.find(user)?.serviceResponse()
                    ?: ErrorInfo("Google account not linked").serviceResponse()
            }.map {
                googleCredentialsRepo.delete(it)
            }
    }

    @Transactional
    fun setBasic(
        user: UserEntity, username: String?,
        oldPassword: String, newPassword: String?
    ): ServiceResponse<BasicCredentialsEntity> {
        val provider = basicUserCredentialsProvider

        val credentials = provider.find(entityManager.merge(user))
            ?: return ErrorInfo("No basic credentials found").serviceResponse()

        if (!provider.check(credentials, oldPassword))
            return ErrorInfo("Old password is incorrect").serviceResponse()

        username?.also { credentials.username = it }
        newPassword?.also { credentials.password = passwordEncoder.encode(newPassword) }
        return basicCredentialsRepo.save(credentials).serviceResponse()
    }

    @Transactional
    fun createBasic(
        user: UserEntity,
        username: String,
        password: String
    ): ServiceResponse<BasicCredentialsEntity> {
        val userEntity = entityManager.merge(user)
        return basicUserCredentialsProvider.create<BasicCredentialsEntity>(
            user = userEntity,
            context = CredentialsCreationContext.createForBasic(
                username = username,
                password = password
            )
        ).serviceResponse()
    }
}