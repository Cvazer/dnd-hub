package com.gitlab.cvazer.dnd.client.microservice.core.security.credintials

import com.gitlab.cvazer.dnd.client.microservice.core.dao.entities.GoogleCredentialsEntity
import com.gitlab.cvazer.dnd.client.microservice.core.dao.entities.UserEntity
import com.gitlab.cvazer.dnd.client.microservice.core.dao.repo.GoogleCredentialsRepo
import com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.model.CredentialsCreationContext
import com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.model.CredentialsProvider
import com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.model.UserCredentials
import com.gitlab.cvazer.dnd.client.microservice.core.service.credentials.model.UserCredentialsDto
import org.springframework.stereotype.Component
import jakarta.transaction.Transactional

@Component
class GoogleUserCredentialsProvider(
    private val credentialsRepo: GoogleCredentialsRepo
): UserCredentialsProvider<Nothing, GoogleCredentialsEntity> {
    override val provider: CredentialsProvider = CredentialsProvider.GOOGLE

    override fun convert(credentials: GoogleCredentialsEntity): UserCredentialsDto {
        return UserCredentialsDto(
            provider = CredentialsProvider.GOOGLE,
            exists = true,
            data = mapOf(
                Pair("googleId", credentials.username)
            )
        )
    }

    override fun find(username: String): GoogleCredentialsEntity? {
        return credentialsRepo.findByUsername(username)
    }

    override fun find(user: UserEntity): GoogleCredentialsEntity? {
        return credentialsRepo.findByUser(user)
    }

    override fun check(credentials: UserCredentials, data: Nothing): Boolean {
        if (credentials !is GoogleCredentialsEntity) error("Invalid credentials type")
        return true
    }

    @Transactional
    @Suppress("UNCHECKED_CAST")
    override fun <T : UserCredentials> create(user: UserEntity, context: CredentialsCreationContext): T {
        return credentialsRepo.save(GoogleCredentialsEntity(
            user = user,
            username = context.username
        )) as T
    }

}