package com.gitlab.cvazer.dnd.client.microservice.core

import com.gitlam.cvazer.dnd.client.microservice.runner.responseEntity
import com.gitlam.cvazer.dnd.client.microservice.runner.serviceResponse
import com.gitlam.cvazer.dnd.client.microservice.runner.web.comms.ServiceResponse
import org.springframework.http.ResponseEntity
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.*

@RestController
class CheckController {

    @GetMapping("/api/core/check")
    fun loginBasic(): ResponseEntity<ServiceResponse<Any>> {
        return SecurityContextHolder.getContext()
            .authentication
            .principal
            .serviceResponse()
            .responseEntity()
    }

}