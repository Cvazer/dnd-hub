package com.gitlab.cvazer.dnd.client.microservice.core.security.service

import com.gitlab.cvazer.dnd.client.microservice.core.dao.entities.GoogleCredentialsEntity
import com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.GoogleUserCredentialsProvider
import com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.model.CredentialsCreationContext
import com.gitlab.cvazer.dnd.client.microservice.core.service.user.UserService
import com.gitlab.cvazer.dnd.client.microservice.core.service.user.model.UserCreationContext
import com.gitlab.cvazer.dnd.core.api.exception.AuthenticationException
import com.gitlab.cvazer.dnd.core.api.service.impl.AbstractAuthenticationService
import com.gitlab.cvazer.dnd.core.model.user.User
import com.nimbusds.jwt.SignedJWT
import org.springframework.stereotype.Service

@Service
class GoogleAuthenticationService(
    private val userCredentialsProvider: GoogleUserCredentialsProvider,
    private val googleOauthService: GoogleOauthService,
    private val userService: UserService
): AbstractAuthenticationService<String, Any>() {

    override fun fetchUserByAuthData(data: String): User? {
        val googleToken = googleOauthService.getAuthentication(data)
            .orThrow { AuthenticationException(it.description!!) }

        val googleClaims = try {
            SignedJWT.parse(googleToken.idToken).jwtClaimsSet
        } catch (e: Exception) {
            throw AuthenticationException(e)
        }

        return userCredentialsProvider.find(googleClaims.subject)?.user
            ?: userService.createUserWithCredentials<GoogleCredentialsEntity>(
                UserCreationContext(googleClaims.getClaim("email").toString()),
                CredentialsCreationContext.createForGoogle(googleClaims.subject)
            ).orThrow {
                AuthenticationException(it.description!!)
            }.user
    }

    override fun validateAuthenticationData(data: String) {}

}