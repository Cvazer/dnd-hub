package com.gitlab.cvazer.dnd.client.microservice.core.security.model

import com.gitlab.cvazer.dnd.core.model.user.User
import com.gitlam.cvazer.dnd.client.microservice.runner.security.model.CoreAuthentication
import com.nimbusds.jwt.JWTClaimsSet

class ClaimsPersistingCoreAuthentication(
    user: User,
    token: String,
    val claims: JWTClaimsSet
) : CoreAuthentication(user, token)