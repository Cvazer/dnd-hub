package com.gitlab.cvazer.dnd.client.microservice.core.security.service

import com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.BasicUserCredentialsProvider
import com.gitlab.cvazer.dnd.client.microservice.core.security.model.BasicAuthData
import com.gitlab.cvazer.dnd.core.api.exception.AuthenticationException
import com.gitlab.cvazer.dnd.core.api.service.impl.AbstractAuthenticationService
import com.gitlab.cvazer.dnd.core.model.user.User
import org.springframework.stereotype.Service

@Service
class BasicAuthenticationService(
    private val userCredentialsProvider: BasicUserCredentialsProvider
): AbstractAuthenticationService<BasicAuthData, Any>() {

    override fun fetchUserByAuthData(data: BasicAuthData): User? {
        val credentials = userCredentialsProvider.find(data.username)
            ?: throw AuthenticationException("Invalid username or password")

        if (!userCredentialsProvider.check(credentials, data.password))
            throw AuthenticationException("Invalid username or password")

        return credentials.user
    }

    override fun validateAuthenticationData(data: BasicAuthData) {}

}