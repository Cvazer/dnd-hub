package com.gitlab.cvazer.dnd.client.microservice.core.security.credintials

import com.gitlab.cvazer.dnd.client.microservice.core.dao.entities.BasicCredentialsEntity
import com.gitlab.cvazer.dnd.client.microservice.core.dao.entities.UserEntity
import com.gitlab.cvazer.dnd.client.microservice.core.dao.repo.BasicCredentialsRepo
import com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.model.CredentialsCreationContext
import com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.model.CredentialsProvider
import com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.model.UserCredentials
import com.gitlab.cvazer.dnd.client.microservice.core.service.credentials.model.UserCredentialsDto
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component
import java.lang.IllegalArgumentException
import jakarta.transaction.Transactional

@Component
class BasicUserCredentialsProvider(
    private val basicCredentialsRepo: BasicCredentialsRepo,
    private val passwordEncoder: PasswordEncoder
): UserCredentialsProvider<String, BasicCredentialsEntity> {
    override val provider: CredentialsProvider = CredentialsProvider.BASIC

    override fun convert(credentials: BasicCredentialsEntity): UserCredentialsDto {
        return UserCredentialsDto(
            provider = CredentialsProvider.BASIC,
            exists = true,
            data = mapOf(
                Pair("username", credentials.username)
            )
        )
    }

    override fun find(username: String): BasicCredentialsEntity? {
        return basicCredentialsRepo.findByUsername(username)
    }

    override fun find(user: UserEntity): BasicCredentialsEntity? {
        return basicCredentialsRepo.findByUser(user)
    }

    override fun check(credentials: UserCredentials, data: String): Boolean {
        val basicCredentials = if (credentials is BasicCredentialsEntity) credentials
            else error("Invalid credentials type")
        return passwordEncoder.matches(
            data, basicCredentials.password
        )
    }

    @Suppress("UNCHECKED_CAST")
    @Transactional
    override fun <T : UserCredentials> create(user: UserEntity, context: CredentialsCreationContext): T {
        if (context.password == null) throw IllegalArgumentException("Password can't be null")
        return basicCredentialsRepo.save(BasicCredentialsEntity(
            user = user,
            username = context.username,
            password = passwordEncoder.encode(context.password)
        )) as T
    }

}