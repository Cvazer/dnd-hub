package com.gitlab.cvazer.dnd.client.microservice.core.security.web

import com.gitlab.cvazer.dnd.client.microservice.core.responseEntity
import com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.model.CredentialsProvider
import com.gitlab.cvazer.dnd.client.microservice.core.security.model.BasicAuthData
import com.gitlab.cvazer.dnd.client.microservice.core.security.service.BasicAuthenticationService
import com.gitlab.cvazer.dnd.client.microservice.core.security.service.GoogleAuthenticationService
import com.gitlab.cvazer.dnd.client.microservice.core.security.service.GoogleOauthService
import com.gitlab.cvazer.dnd.client.microservice.core.security.service.JwtTokenService
import com.gitlab.cvazer.dnd.client.microservice.core.serviceResponse
import com.gitlab.cvazer.dnd.client.microservice.core.trueOrError
import com.gitlam.cvazer.dnd.client.microservice.runner.responseEntity
import com.gitlam.cvazer.dnd.client.microservice.runner.web.comms.ErrorInfo
import com.gitlam.cvazer.dnd.client.microservice.runner.web.comms.ServiceResponse
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.net.URLDecoder
import java.nio.charset.Charset

@RestController
@RequestMapping("/core/login")
class LoginController(
    private val googleOauthService: GoogleOauthService,
    private val jwtTokenService: JwtTokenService,
    private val basicAuthenticationService: BasicAuthenticationService,
    private val googleAuthenticationService: GoogleAuthenticationService
) {

    @PostMapping("/basic")
    fun loginBasic(@RequestBody credentials: BasicAuthData): ResponseEntity<ServiceResponse<String>> =
        basicAuthenticationService
            .authenticate(credentials)
            .serviceResponse()
            .flatMap {
                jwtTokenService.generate(
                    it.second.id!!.toString(),
                    CredentialsProvider.BASIC.name.lowercase(),
                    it.first.rememberMe
                )
            }.responseEntity(HttpStatus.UNAUTHORIZED)

    @PostMapping("/refresh")
    fun refreshToken(@RequestBody token: String): ResponseEntity<ServiceResponse<String>> =
        jwtTokenService.parseWithoutValidatingClaims(token)
            .trueOrError(ErrorInfo("Token is not refreshable")) {
                it.getClaim("ref") == "false"
            }.flatMap { claims ->
                jwtTokenService.generate(
                    subject = claims.subject,
                    provider = claims.getClaim("prv").toString(),
                    refreshable = true
                )
            }.responseEntity()

    @GetMapping("/google")
    fun getGoogleLoginUrl(): ServiceResponse<String> = googleOauthService
        .getLoginUrl()

    @PostMapping("/google")
    fun loginGoogle(@RequestBody userCode: String): ResponseEntity<ServiceResponse<String>> {
        return googleAuthenticationService
            .authenticate(URLDecoder.decode(userCode, Charset.defaultCharset()))
            .serviceResponse()
            .flatMap {
                jwtTokenService.generate(
                    it.second.id!!.toString(),
                    CredentialsProvider.GOOGLE.name.lowercase(),
                    true
                )
            }.responseEntity(HttpStatus.UNAUTHORIZED)
    }
}