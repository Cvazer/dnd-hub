package com.gitlab.cvazer.dnd.client.microservice.core.dao

import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter
import java.util.*
import javax.sql.DataSource


@Configuration
class CoreDatasourceConfig(
    private val coreDatasourceProperties: CoreDatasourceProperties,
    private val coreHibernateProperties: CoreHibernateProperties
) {

    @Bean
    fun getDataSource(): DataSource {
        return DataSourceBuilder.create()
//            .apply { driverClassName("org.h2.Driver") }
            .apply { url(coreDatasourceProperties.url) }
            .apply { username(coreDatasourceProperties.username) }
            .apply { password(coreDatasourceProperties.password) }
            .build()
    }

    @Bean
    fun entityManagerFactory(dataSource: DataSource): LocalContainerEntityManagerFactoryBean {
        return LocalContainerEntityManagerFactoryBean()
            .apply { setDataSource(dataSource) }
            .apply { setPackagesToScan("com.gitlab.cvazer.dnd.client.microservice.core") }
            .apply { jpaVendorAdapter = HibernateJpaVendorAdapter() }
            .apply { setJpaProperties(
                Properties()
//                    .apply { setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect") }
                    .apply { setProperty("hibernate.hbm2ddl.auto", coreHibernateProperties.ddlAuto) }
            ) }
    }
}