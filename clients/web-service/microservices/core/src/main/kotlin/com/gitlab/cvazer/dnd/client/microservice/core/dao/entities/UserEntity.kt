package com.gitlab.cvazer.dnd.client.microservice.core.dao.entities

import com.gitlab.cvazer.dnd.core.model.user.User
import java.time.OffsetDateTime
import jakarta.persistence.*

@Entity(name = "User")
@Table(name = "users", schema = "core")
class UserEntity(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    override var id: Long? = null,

    override var nickname: String,
    override var created: OffsetDateTime = OffsetDateTime.now()
): User