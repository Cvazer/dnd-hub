package com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.model

import com.gitlab.cvazer.dnd.core.model.user.User

interface UserCredentials {
    val user: User
    val username: String
}