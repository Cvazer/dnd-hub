package com.gitlab.cvazer.dnd.client.microservice.core.security

import com.gitlab.cvazer.dnd.client.microservice.core.security.config.CoreSecurityProperties
import com.nimbusds.jose.JOSEException
import com.nimbusds.jose.jwk.JWK
import com.nimbusds.jose.jwk.RSAKey
import com.nimbusds.jose.jwk.gen.RSAKeyGenerator
import org.springframework.stereotype.Component
import java.io.File
import java.io.IOException
import java.util.*

@Component
class RsaKeyProvider(
    coreSecurityProperties: CoreSecurityProperties
) {
    private val _generatedKey: RSAKey by lazy {
        RSAKeyGenerator(2048).keyID(UUID.randomUUID().toString()).generate()
    }

    val publicRSAKey: RSAKey = loadKeyFromFile(coreSecurityProperties.keys.pkcs8PublicFilePath) { it.toPublicJWK() }
    val privateRSAKey: RSAKey = loadKeyFromFile(coreSecurityProperties.keys.pkcs8PrivateFilePath) { it }

    private fun loadKeyFromFile(filePath: String, fallback: (key: RSAKey) -> RSAKey): RSAKey =
        try {
            JWK.parseFromPEMEncodedObjects(File(filePath).readText()) as RSAKey
        } catch (e: Exception) {
            when (e) {
                is IOException,
                is JOSEException -> fallback.invoke(_generatedKey)
                else -> throw e
            }
        }
}