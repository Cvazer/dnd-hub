package com.gitlab.cvazer.dnd.client.microservice.core.service.user

import com.gitlab.cvazer.dnd.client.microservice.core.dao.entities.UserEntity
import com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.UserCredentialsProvider
import com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.UserCredentialsProviderRegistry
import com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.model.CredentialsCreationContext
import com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.model.UserCredentials
import com.gitlab.cvazer.dnd.client.microservice.core.service.credentials.model.UserCredentialsDto
import com.gitlab.cvazer.dnd.client.microservice.core.service.user.model.UserCreationContext
import com.gitlam.cvazer.dnd.client.microservice.runner.log
import com.gitlam.cvazer.dnd.client.microservice.runner.serviceResponse
import com.gitlam.cvazer.dnd.client.microservice.runner.web.comms.ErrorInfo
import com.gitlam.cvazer.dnd.client.microservice.runner.web.comms.ServiceResponse
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.stereotype.Service

@Service
class UserService(
    private val credentialsProviderRegistry: UserCredentialsProviderRegistry,
) {

    fun getAllCredentialsForUser(user: UserEntity): ServiceResponse<List<UserCredentialsDto>> {
        return credentialsProviderRegistry.getAll()
            .map { provider ->
                val credentials = provider.find(user)
                if (credentials == null) {
                    UserCredentialsDto(provider = provider.provider)
                } else {
                    provider.convert(credentials)
                }
            }.serviceResponse()
    }

    fun <T: UserCredentials> createUserWithCredentials(
        userContext: UserCreationContext,
        credentialsContext: CredentialsCreationContext
    ): ServiceResponse<T> {
        val providerService: UserCredentialsProvider<*, *> = credentialsProviderRegistry[credentialsContext.provider]
            ?: return ErrorInfo("Invalid provider ${credentialsContext.provider}").serviceResponse()

        return try {
            providerService.create<T>(
                UserEntity(nickname = userContext.nickname),
                credentialsContext
            ).serviceResponse()
        } catch (e: DataIntegrityViolationException) {
            if (e.mostSpecificCause.message!!.contains(userContext.nickname)) {
                ErrorInfo("User '${userContext.nickname}' already exists")
                    .serviceResponse<T>()
                    .also { log().debug(it.errorInfo.description) }
            } else throw e
        }
    }

}