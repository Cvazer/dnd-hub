package com.gitlab.cvazer.dnd.client.microservice.core.security.service//package com.gitlab.cvazer.dnd.client.microservice.core.security.service

import com.fasterxml.jackson.annotation.JsonProperty
import com.gitlab.cvazer.dnd.client.microservice.core.security.config.CoreSecurityProperties
import com.gitlam.cvazer.dnd.client.microservice.runner.serviceResponse
import com.gitlam.cvazer.dnd.client.microservice.runner.web.comms.ErrorInfo
import com.gitlam.cvazer.dnd.client.microservice.runner.web.comms.ServiceResponse
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod.POST
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder

@Service
class GoogleOauthService(
    private val coreSecurityProperties: CoreSecurityProperties
) {

//    @Value("\${spring.security.oauth2.client.registration.google.client-id}")
//    private lateinit var clientId: String
//
//    @Value("\${spring.security.oauth2.client.registration.google.client-secret}")
//    private lateinit var clientSecret: String
//
//    @Value("\${spring.security.oauth2.client.registration.google.scope}")
//    private lateinit var scope: String
//
//    @Value("\${spring.security.oauth2.client.registration.google.redirect-uri}")
//    private lateinit var redirectUri: String

    fun getLoginUrl(): ServiceResponse<String> = UriComponentsBuilder
        .newInstance()
        .scheme("https")
        .host("accounts.google.com")
        .path("/o/oauth2/auth")
        .queryParam("client_id", coreSecurityProperties.google.clientId)
        .queryParam("redirect_uri", coreSecurityProperties.google.redirectUri)
        .queryParam("scope", coreSecurityProperties.google.scope)
        .queryParam("response_type", "code")
        .build()
        .encode()
        .toUriString()
        .serviceResponse()

    fun getAuthentication(userCode: String): ServiceResponse<GoogleTokenAuthResponse> {
        return try {
            authenticateUserCode(userCode).serviceResponse()
        } catch (e: Exception) {
            ErrorInfo(e).serviceResponse()
        }
    }

    private fun authenticateUserCode(userCode: String): GoogleTokenAuthResponse {
        val uri = UriComponentsBuilder
            .newInstance()
            .scheme("https")
            .host("accounts.google.com")
            .path("/o/oauth2/token")
            .queryParam("grant_type", "authorization_code")
            .queryParam("code", userCode)
            .queryParam("client_secret", coreSecurityProperties.google.clientSecret)
            .queryParam("client_id", coreSecurityProperties.google.clientId)
            .queryParam("redirect_uri", coreSecurityProperties.google.redirectUri)
            .build()
            .encode()
            .toUriString()

        val requestEntity: HttpEntity<String> = HttpEntity(
            HttpHeaders().apply { set("Content-Length", "0") }
        )

        return RestTemplate().exchange(uri, POST, requestEntity, GoogleTokenAuthResponse::class.java).body!!
    }

    data class GoogleTokenAuthResponse(
        @JsonProperty("access_token") val accessToken: String,
//        @JsonProperty("refresh_token") var refreshToken: String?,
        @JsonProperty("id_token") val idToken: String
    )
}