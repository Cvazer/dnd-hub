package com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.model

enum class CredentialsProvider {
    GOOGLE, BASIC
}