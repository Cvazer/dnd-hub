package com.gitlab.cvazer.dnd.client.microservice.core.security.model

data class BasicAuthData(
    val username: String,
    val password: String,
    val rememberMe: Boolean = false
)