package com.gitlab.cvazer.dnd.client.microservice.core.dao.repo

import com.gitlab.cvazer.dnd.client.microservice.core.dao.entities.UserEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepo: JpaRepository<UserEntity, Long>