package com.gitlab.cvazer.dnd.client.microservice.core.security.credintials

import com.gitlab.cvazer.dnd.client.microservice.core.dao.entities.UserEntity
import com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.model.CredentialsCreationContext
import com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.model.CredentialsProvider
import com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.model.UserCredentials
import com.gitlab.cvazer.dnd.client.microservice.core.service.credentials.model.UserCredentialsDto

interface UserCredentialsProvider<D, R: UserCredentials> {
    val provider: CredentialsProvider

    fun convert(credentials: R): UserCredentialsDto
    fun find(username: String): R?
    fun find(user: UserEntity): R?
    fun check(credentials: UserCredentials, data: D): Boolean
    fun <T: UserCredentials> create(user: UserEntity, context: CredentialsCreationContext): T
}