package com.gitlab.cvazer.dnd.client.microservice.core.service.user.web

import com.gitlab.cvazer.dnd.client.microservice.core.dao.entities.UserEntity
import com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.model.CredentialsCreationContext
import com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.model.UserCredentials
import com.gitlab.cvazer.dnd.client.microservice.core.service.user.UserService
import com.gitlab.cvazer.dnd.client.microservice.core.service.user.model.UserCreationContext
import com.gitlam.cvazer.dnd.client.microservice.runner.responseEntity
import com.gitlam.cvazer.dnd.client.microservice.runner.serviceResponse
import com.gitlam.cvazer.dnd.client.microservice.runner.web.ErrorControllerAdvice
import com.gitlam.cvazer.dnd.client.microservice.runner.web.comms.ServiceResponse
import jakarta.validation.Valid
import jakarta.validation.constraints.*
import org.springframework.http.ResponseEntity
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.validation.FieldError
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class UserController(
    private val userService: UserService
) {

    @PostMapping("/core/login/user")
    fun create(@RequestBody @Valid rq: CreateUserRq): ResponseEntity<ServiceResponse<*>> {
        if (!rq.password.contentEquals(rq.repeatPassword)) {
            return ErrorControllerAdvice.createValidationResponse(listOf(FieldError(
                "createUserRq", "repeatPassword", "Passwords do not match"
            ))).responseEntity()
        }
        return userService.createUserWithCredentials<UserCredentials>(
            UserCreationContext(nickname = rq.username),
            CredentialsCreationContext.createForBasic(
                username = rq.username,
                password = rq.password
            )
        ).map{}.responseEntity()
    }

    data class CreateUserRq(
        @NotBlank
        @Pattern(regexp = "^[a-zA-Z0-9@.]+$")
        @Min(4L, message = "Username must be at least 4 characters long")
        @Max(48L, message = "Username can be at most 48 characters long")
        val username: String,

        @NotBlank
        @Pattern(regexp = "^[a-zA-Z0-9._ &$%*!@^=+-]+$")
        @Min(6L, message = "Password must be at least 6 symbols long")
        @Max(48L, message = "Password can be at most 48 symbols long")
        val password: String,

        val repeatPassword: String
    )

    @GetMapping("/api/core/user")
    fun getByAuth(): ServiceResponse<UserEntity> {
        return SecurityContextHolder.getContext().authentication
            .principal
            .let { it as UserEntity }
            .serviceResponse()
    }
}