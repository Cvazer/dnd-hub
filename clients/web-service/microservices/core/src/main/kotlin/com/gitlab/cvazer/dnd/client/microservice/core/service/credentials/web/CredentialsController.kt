package com.gitlab.cvazer.dnd.client.microservice.core.service.credentials.web

import com.gitlab.cvazer.dnd.client.microservice.core.dao.entities.UserEntity
import com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.BasicUserCredentialsProvider
import com.gitlab.cvazer.dnd.client.microservice.core.security.credintials.GoogleUserCredentialsProvider
import com.gitlab.cvazer.dnd.client.microservice.core.service.credentials.CredentialsService
import com.gitlab.cvazer.dnd.client.microservice.core.service.credentials.model.UserCredentialsDto
import com.gitlab.cvazer.dnd.client.microservice.core.service.user.UserService
import com.gitlab.cvazer.dnd.client.microservice.core.service.user.web.UserController
import com.gitlam.cvazer.dnd.client.microservice.runner.web.comms.ServiceResponse
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/core/credentials")
class CredentialsController(
    private val userService: UserService,
    private val credentialsService: CredentialsService,
    private val googleUserCredentialsProvider: GoogleUserCredentialsProvider,
    private val basicUserCredentialsProvider: BasicUserCredentialsProvider,
) {

    @GetMapping
    fun getByAuth(): ServiceResponse<List<UserCredentialsDto>> =
        SecurityContextHolder.getContext().authentication
            .principal
            .let { it as UserEntity }
            .let { userService.getAllCredentialsForUser(it)  }

    @PostMapping("/basic")
    fun createBasic(@RequestBody rq: UserController.CreateUserRq): ServiceResponse<UserCredentialsDto> =
        credentialsService.createBasic(
            user = SecurityContextHolder.getContext().authentication
                .principal as UserEntity,
            username = rq.username,
            password = rq.password
        ).map {
            basicUserCredentialsProvider.convert(it)
        }

    @PutMapping("/basic")
    fun setBasic(@RequestBody rq: SetBasicCredentialsRq): ServiceResponse<UserCredentialsDto> =
        credentialsService.setBasic(
            user = SecurityContextHolder.getContext()
                .authentication.principal as UserEntity,
            username = rq.username,
            oldPassword = rq.oldPassword,
            newPassword = rq.newPassword
        ).map {
            basicUserCredentialsProvider.convert(it)
        }

    @PostMapping("/google")
    fun linkGoogle(@RequestBody code: String): ServiceResponse<UserCredentialsDto> {
        return SecurityContextHolder.getContext().authentication
            .principal
            .let { credentialsService.linkGoogleAccount(it as UserEntity, code) }
            .map { googleUserCredentialsProvider.convert(it) }
    }

    @DeleteMapping("/google")
    fun disconnectGoogle(): ServiceResponse<Unit> =
        SecurityContextHolder.getContext().authentication
            .principal
            .let { credentialsService.disconnectGoogle(it as UserEntity) }

    data class SetBasicCredentialsRq(
        val oldPassword: String,
        val newPassword: String?,
        val username: String?
    )
}