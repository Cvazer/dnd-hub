package com.gitlam.cvazer.dnd.client.microservice.runner.security.model

import com.gitlab.cvazer.dnd.core.model.user.User
import org.springframework.security.authentication.AbstractAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority

open class CoreAuthentication(
    private val user: User,
    private val token: String
): AbstractAuthenticationToken(mutableListOf(
    SimpleGrantedAuthority("USER")
)) {
    override fun getCredentials(): Any = token
    override fun getPrincipal(): Any = user
}