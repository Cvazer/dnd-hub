package com.gitlam.cvazer.dnd.client.microservice.runner.transport

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.bind.ConstructorBinding


@ConfigurationProperties("transport.amqp")
open class AmqpProperties {
    var username: String
    var password: String
    var host: String
    var port: Int

    @Suppress("ConvertSecondaryConstructorToPrimary")
    @ConstructorBinding
    constructor(
        username: String = "noop",
        password: String = "noop",
        host: String = "localhost",
        port: Int = 5672
    ) {
        this.username = username
        this.password = password
        this.host = host
        this.port = port
    }
}