package com.gitlam.cvazer.dnd.client.microservice.runner.web.comms

import com.fasterxml.jackson.annotation.JsonIgnore
import com.gitlam.cvazer.dnd.client.microservice.runner.serviceResponse

open class ServiceResponse<T> {
    @JsonIgnore private var _errorInfo: ErrorInfo
    @JsonIgnore private var _data: T? = null

    val errorInfo: ErrorInfo
        get() { return _errorInfo }

    val data: T?
        get() { return _data }

    constructor(errorInfo: ErrorInfo, data: T?) {
        this._errorInfo = errorInfo
        this._data = data
    }

    /**
     * Should only be used in case of error!
     */
    constructor(errorInfo: ErrorInfo): this(errorInfo, null) {
        if (errorInfo.isSuccess()) error(ERROR_INFO_CONSTRUCTOR_ONLY_ON_ERROR)
    }

    constructor(data: T): this(ErrorInfo(), data)

    fun <R> map(block: (data: T) -> R): ServiceResponse<R> =
        if (isSuccess()) block.invoke(_data!!).serviceResponse()
        else _errorInfo.serviceResponse()

    fun <R> flatMap(block: (data: T) -> ServiceResponse<R>): ServiceResponse<R> =
        if (isSuccess()) block.invoke(_data!!)
        else _errorInfo.serviceResponse()

    fun orElse(block: (errorInfo: ErrorInfo) -> T): T =
        if (isFailure()) block.invoke(_errorInfo)
        else _data!!

    fun orThrow(block: (errorInfo: ErrorInfo) -> Exception): T =
        if (isFailure()) throw block.invoke(_errorInfo)
        else _data!!

    fun onFailure(block: (errorInfo: ErrorInfo) -> Void): ServiceResponse<T> =
        if (isSuccess()) this
        else { block.invoke(_errorInfo); this }

    @JsonIgnore
    fun isFailure(): Boolean {
        return !isSuccess()
    }

    @JsonIgnore
    fun isSuccess(): Boolean {
        return _data != null
    }

    companion object {
        const val ERROR_INFO_CONSTRUCTOR_ONLY_ON_ERROR =
            "Constructor with errorInfo should only be used in case of error. " +
            "Use constructor with result instead in case of OK operation"
    }
}