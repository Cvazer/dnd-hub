package com.gitlam.cvazer.dnd.client.microservice.runner.security

import com.gitlam.cvazer.dnd.client.microservice.runner.web.comms.ServiceResponse
import org.springframework.security.core.Authentication

interface TokenService {
    fun generate(subject: String, provider: String, refreshable: Boolean): ServiceResponse<String>
    fun checkSignature(token: String): ServiceResponse<Boolean>
    fun validate(token: String): ServiceResponse<Authentication>
}