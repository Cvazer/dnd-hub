package com.gitlam.cvazer.dnd.client.microservice.runner.security.config

import com.gitlam.cvazer.dnd.client.microservice.runner.security.CustomJwtAuthenticationManager
import com.gitlam.cvazer.dnd.client.microservice.runner.security.FilterChainMixin
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.web.SecurityFilterChain


@Configuration
@EnableWebSecurity
@EnableMethodSecurity(prePostEnabled = true)
@PropertySource(
    value = ["classpath:security.properties", "file:security.properties"],
    ignoreResourceNotFound = true
)
class SpringSecurityConfig (
    private val customJwtAuthenticationManager: CustomJwtAuthenticationManager,
    private val authEntryPoint: DelegatedAuthenticationEntryPoint,
    private val mixins: List<FilterChainMixin>
) {

    @Bean
    fun jwtAuthFilterChain(http: HttpSecurity): SecurityFilterChain {
        val intermediate = http
            .httpBasic().disable()
            .logout().disable()
            .formLogin().disable()
            .csrf().disable()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
                .oauth2ResourceServer()
                .authenticationManagerResolver { customJwtAuthenticationManager }
                .authenticationEntryPoint(authEntryPoint).and()
            .cors().and()
            .authorizeHttpRequests()
            .requestMatchers(HttpMethod.OPTIONS, "/**").anonymous()
            .requestMatchers(HttpMethod.HEAD, "/**").anonymous()
            .requestMatchers("/error").permitAll()

        mixins.forEach { it.mix(intermediate) }

        return http.build()
    }
}