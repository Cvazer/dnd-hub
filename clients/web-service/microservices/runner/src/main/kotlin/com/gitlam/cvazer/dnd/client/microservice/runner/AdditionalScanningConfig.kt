package com.gitlam.cvazer.dnd.client.microservice.runner

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration

@Configuration
@ComponentScan("com.gitlab.cvazer.dnd")
class AdditionalScanningConfig