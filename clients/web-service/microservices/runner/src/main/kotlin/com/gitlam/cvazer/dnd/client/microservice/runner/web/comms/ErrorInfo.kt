package com.gitlam.cvazer.dnd.client.microservice.runner.web.comms

import com.fasterxml.jackson.annotation.JsonIgnore

data class ErrorInfo(
    val code: Int,
    val description: String?
) {

    constructor(): this(0, "OK")
    constructor(description: String?): this(1, description)
    constructor(e: Exception): this(e.message)
    constructor(code: Int, e: Exception): this(code, e.message)

    @JsonIgnore
    fun isSuccess(): Boolean =
        code == 0 && (description == null || description == "OK")
}