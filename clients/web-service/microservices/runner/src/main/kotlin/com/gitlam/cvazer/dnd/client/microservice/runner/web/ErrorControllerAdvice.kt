package com.gitlam.cvazer.dnd.client.microservice.runner.web

import com.gitlam.cvazer.dnd.client.microservice.runner.log
import com.gitlam.cvazer.dnd.client.microservice.runner.responseEntity
import com.gitlam.cvazer.dnd.client.microservice.runner.serviceResponse
import com.gitlam.cvazer.dnd.client.microservice.runner.web.comms.ErrorInfo
import com.gitlam.cvazer.dnd.client.microservice.runner.web.comms.ServiceResponse
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.validation.FieldError
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
class ErrorControllerAdvice {

    @ExceptionHandler(BadCredentialsException::class)
    fun jwtExpired(e: BadCredentialsException): ResponseEntity<ServiceResponse<Unit>> =
        if (e.message == "Expired JWT") {
            log().debug(e.message, e)
            ResponseEntity(
                ErrorInfo(499, e.message!!).serviceResponse(),
                HttpStatus.OK
            )
        } else {
            log().error(e.message, e)
            ResponseEntity(
                ErrorInfo(1, e.message!!).serviceResponse(),
                HttpStatus.OK
            )
        }

    @ExceptionHandler(MethodArgumentNotValidException::class)
    fun bindException(e: MethodArgumentNotValidException): ResponseEntity<ServiceResponse<List<BindingError>>> =
        createValidationResponse(e.fieldErrors.map { it as FieldError }).responseEntity()

    @ExceptionHandler(Exception::class)
    fun fallback(e: Exception): ResponseEntity<ServiceResponse<Unit>> = e
        .apply { log().error(e.message, e) }
        .let {
            ResponseEntity(
                ErrorInfo(1, e.message!!).serviceResponse(),
                HttpStatus.OK
            )
        }

    companion object {
        data class BindingError(
            val fieldName: String,
            val error: String
        )

        fun createValidationResponse(errors: List<FieldError>): ServiceResponse<List<BindingError>> {
            return ServiceResponse(
                ErrorInfo("Validation failed"),
                errors.map { BindingError(it.field, it.defaultMessage!!) }
            )
        }
    }
}