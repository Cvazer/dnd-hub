package com.gitlam.cvazer.dnd.client.microservice.runner

import com.fasterxml.jackson.annotation.JsonFilter
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider
import com.gitlam.cvazer.dnd.client.microservice.runner.web.comms.ErrorInfo
import com.gitlam.cvazer.dnd.client.microservice.runner.web.comms.ServiceResponse
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

fun <T> T.serviceResponse(): ServiceResponse<T> = ServiceResponse(this)
fun <T> ErrorInfo.serviceResponse(): ServiceResponse<T> = ServiceResponse(this)
fun <T> ServiceResponse<T>.serviceResponse(): ServiceResponse<T> = this

fun <T> T.responseEntity(): ResponseEntity<T> = this.responseEntity(HttpStatus.OK)
fun <T> T.responseEntity(status: HttpStatus): ResponseEntity<T> = ResponseEntity(this, status)

@Suppress("UnusedReceiverParameter")
inline fun <reified T> T.log(): Logger = LoggerFactory.getLogger(T::class.java)

object Lang {
    val mapper: ObjectMapper = JsonMapper.builder()
        .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
        .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
        .serializationInclusion(JsonInclude.Include.NON_NULL)
        .findAndAddModules()
        .addMixIn(Any::class.java, DynamicMixIn::class.java)
        .filterProvider(
            SimpleFilterProvider()
            .addFilter("dynamicFilter",
                SimpleBeanPropertyFilter
                    .serializeAllExcept("hibernateLazyInitializer", "handler")))
        .build()


    @JsonFilter("dynamicFilter")
    class DynamicMixIn
}