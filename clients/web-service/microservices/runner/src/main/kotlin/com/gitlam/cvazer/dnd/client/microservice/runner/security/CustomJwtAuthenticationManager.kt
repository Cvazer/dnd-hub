package com.gitlam.cvazer.dnd.client.microservice.runner.security

import com.nimbusds.jose.proc.BadJOSEException
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.core.Authentication
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthenticationToken
import org.springframework.stereotype.Component
import java.text.ParseException

@Component
class CustomJwtAuthenticationManager(
    private val tokenService: TokenService
): AuthenticationManager {
    override fun authenticate(authentication: Authentication): Authentication {
        val tokenBearer = if (authentication is BearerTokenAuthenticationToken) authentication
        else return authentication

        return try {
            tokenService.validate(tokenBearer.token).data!!
        } catch (e: BadJOSEException) {
            throw BadCredentialsException(e.message, e)
        } catch (e: ParseException) {
            throw BadCredentialsException(e.message, e)
        }
    }
}