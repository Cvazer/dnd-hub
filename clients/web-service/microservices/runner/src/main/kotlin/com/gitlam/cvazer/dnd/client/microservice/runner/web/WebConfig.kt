package com.gitlam.cvazer.dnd.client.microservice.runner.web

import com.fasterxml.jackson.databind.ObjectMapper
import com.gitlam.cvazer.dnd.client.microservice.runner.Lang
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@EnableWebMvc
@Configuration
class WebConfig: WebMvcConfigurer {

    @Bean
    @Primary
    fun objectMapper(): ObjectMapper = Lang.mapper

    @Bean
    @Primary
    fun mappingJackson2HttpMessageConverter() =
        MappingJackson2HttpMessageConverter(Lang.mapper)

    override fun addCorsMappings(registry: CorsRegistry) {
        registry.addMapping("/**")
            .allowedMethods("*")
            .allowedOrigins("http://localhost:3000")
    }

    override fun configureMessageConverters(converters: MutableList<HttpMessageConverter<*>>) {
        converters.add(0, mappingJackson2HttpMessageConverter())
        super.configureMessageConverters(converters)
    }
}