package com.gitlam.cvazer.dnd.client.microservice.runner.transport

//import org.springframework.amqp.core.Queue
//import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory
import org.springframework.amqp.rabbit.connection.ConnectionFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource


@Configuration
@PropertySource(
    value = ["classpath:transport.properties", "file:transport.properties"],
    ignoreResourceNotFound = true
)
class AmqpConfig(
    private val properties: AmqpProperties
){

    @Bean
    fun connectionFactory(): ConnectionFactory = CachingConnectionFactory()
        .apply { host = properties.host }
        .apply { port = properties.port }
        .apply { username = properties.username }
        .apply { setPassword(properties.password) }

//    referense:
//    @Bean
//    fun myQueue(): Queue {
//        return Queue("myQueue", false)
//    }
//
//    @RabbitListener(queues = ["myQueue"])
//    fun listen(`in`: String) {
//        println("Message read from myQueue : $`in`")
//    }

}